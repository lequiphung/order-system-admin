import * as types from '../constants/types';

export function requestLogin(username, password) {
  return {
    type: types.LOGIN_SUCCESS,
    username, 
	  password
  };
}
  
export function loginSuccess(userInfo) {
  return {
    type: types.LOGIN_SUCCESS,
    userInfo
  };
}

export function loginFailed(message){
  return {
    type: types.LOGIN_FAIL,
    message
  };	
}
export function loginOut(){
  return {
    type: types.LOGIN_SUCCESS
  };
}
