import * as types from '../constants/types';

export function setObjectInfo(object_info) {
    return {
        type: types.PARTNER_SET_OBJECT_INFO,
        object_info
    };
}