import React from "react";
import {MDBDataTableV5} from "mdbreact";
import {articlesServices} from "../../../services/articlesServices";


import { Table } from 'antd';

const List = ({ articles,count }) => {
    const columns = [
        {
            title: '#',
            dataIndex: 'index',
            width: 150,
        },
        {
            title: 'Title',
            dataIndex: 'title',
            width: 270,
        },
        {
            title: 'Image',
            dataIndex: 'image',
            width: 200,
            render: image => <img alt={image} src={image} height="50" />
        },
        {
            title: 'Category',
            dataIndex: 'category',
            width: 100,
        },
        {
            title: 'Status',
            dataIndex: 'status',
            sort: 'disabled',
            width: 150,
        },
        {
            title: 'Action',
            key: 'operation',
            fixed: 'right',
            width: 100,
            render: () => <a>action</a>,
        },
    ];

    let data = [];
    articles.map((article,i) => {
        const imageUrl = process.env.NODE_ENV !== "development"
            ? article.image.url
            : process.env.REACT_APP_BACKEND_URL + article.image.url;
        data.push(
            {
                key: i,
                index:i+1,
                title: article.title,
                image:  imageUrl,
                category: article.category.name,
                status: "Success",
                function: ''
            }
        );
        return data;
    })

    return <Table columns={columns} dataSource={data} />


    console.log(count);

    const [datatable, setDatatable] = React.useState({
        columns: [
            {
                label: '#',
                field: 'index',
                width: 150,
                sort: 'asc',
                // attributes: {
                //     'aria-controls': 'DataTable',
                //     'aria-label': 'Name',
                // },
            },
            {
                label: 'Title',
                field: 'title',
                width: 270,
            },
            {
                label: 'Image',
                field: 'image',
                width: 200,
            },
            {
                label: 'Category',
                field: 'category',

                width: 100,
            },
            {
                label: 'Status',
                field: 'status',
                sort: 'disabled',
                width: 150,
            },
            {
                label: 'Function',
                field: 'function',
                sort: 'disabled',
                width: 100,
            },
        ],
        rows: [
            {
                category: "news",
                function: "",
                image: "http://localhost:1337/uploads/blog_header_network_f9476b44ce.jpg",
                index: 1,
                status: "Success",
                title: "Thanks for giving this Starter a try!"
            }
        ],
    });
    console.log(process.env.customKey);
    let rows = [];
    articles.map((article,i) => {
        const imageUrl = process.env.NODE_ENV !== "development"
            ? article.image.url
            : process.env.REACT_APP_BACKEND_URL + article.image.url;
        rows.push(
            {
                index:i+1,
                title: article.title,
                image: imageUrl,
                category: article.category.name,
                status: "Success",
                function: ''
            }
        );
        return rows;
    })
    datatable.rows = rows;
    console.log(rows);

    return (
        <div>
            <p>{count}</p>
            <MDBDataTableV5
                hover
                entriesOptions={[5, 20, 25]}
                entries={5}
                pagesAmount={4}
                data={datatable}
                pagingTop
                searchTop
                searchBottom={false}
            />
        </div>

    )
};

export default List;
