import Wrapper from "../../Wrapper";
import {Button, Col, Form, Input, notification, Row, Select, Table, Tag, Layout, Modal} from "antd";
import Link from "next/link";
import React, {useEffect, useState} from "react";
import $ from "jquery";
import { SearchOutlined } from '@ant-design/icons';
import NumberFormat from 'react-number-format';
import {formatDataForm} from "../../../function";

const { Content } = Layout;
const { Option } = Select;
const ListCensorshipProduct = (
    {
        products,
        pagination,
        setPagination,
        loadingParent,
        where_default,
        setWhere,
        object_info
    }) => {

    if(!products){
        return <div></div>;
    }
    const [form] = Form.useForm();

    const onFinish = values => {
        const data_form = formatDataForm(values,true);
        setWhere(value => ({...where_default,...data_form}));
    };

    const onClear = () =>{
        form.resetFields();
        setWhere(where_default);
    }

    let rows = [];
    products.products.map((item,i) => {
        let imageUrl = '';
        if(!$.isEmptyObject(item.image)){
            imageUrl = process.env.NODE_ENV !== "development"
                ? item.image.url
                : process.env.API_BACKEND_URL + item.image.url;
        }
        rows.push(
            {
                id:item.id,
                key: item.id,
                index:i+1,
                name: item.name,
                image: imageUrl,
                price: item.price,
                active: item.active,
                shop_name:item?.user_id?.partner_id?.name
            }
        );
        return rows
    });

    const columns = [
        {
            title: '#',
            dataIndex: 'index',
            width: 150,
        },
        {
            title: 'Name',
            dataIndex: 'name',
            width: 270,
        },
        {
            title: 'Image',
            dataIndex: 'image',
            width: 200,
            render: image => <img alt={image} src={image} height="50" />
        },
        {
            title: 'Price',
            dataIndex: 'price',
            width: 100,
            render: price => <NumberFormat value={price} displayType={'text'} thousandSeparator={true}/>
        },
        {
            title: 'Status',
            dataIndex: 'active',
            width: 150,
            render: (text, record) => {
                return text?<Tag color="green">Active</Tag>:<Tag color="red">Inactive</Tag>;
            },
        },
        {
            title: 'Shop',
            dataIndex: 'shop_name',
            width: 200,
        },
        {
            title: 'Action',
            key: 'operation',
            fixed: 'right',
            width: 100,
            render: (text, record) => (
                <div className="action-btns">
                    <Link href={`${object_info.show_prefix}[id]`} as={`${object_info.show_prefix}${record.id}`}>
                        <a className="edit-btn"><i className="fas fa-eye"></i></a>
                    </Link>
                </div>
            ),
        },
    ];

    return (
        <Content>
            <main>
                <div className="container-fluid">
                    <h2 className="mt-30 page-title">{object_info.title}</h2>
                    <ol className="breadcrumb mb-30">
                        <li className="breadcrumb-item">{object_info.title}</li>
                        <li className="breadcrumb-item active">{object_info.active}</li>
                    </ol>
                    <div className="row justify-content-between">

                        <div className="col-lg-12 col-md-12 mt-30">
                            <Form
                                form={form}
                                name="advanced_search"
                                className="ant-advanced-search-form"
                                onFinish={onFinish}
                            >
                                <Row>
                                    <Col span={6}>
                                        <Form.Item
                                            name="active"
                                        >
                                            <Select
                                                showSearch
                                                placeholder="Select a status"
                                                // optionFilterProp="children"
                                                filterOption={(input, option) =>
                                                    option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                                }
                                            >
                                                <Option value="">All</Option>
                                                <Option value="1">Active</Option>
                                                <Option value="0">Inactive</Option>
                                            </Select>
                                        </Form.Item>
                                    </Col>
                                    <Col span={6} style={{marginLeft:"5px"}}>
                                        <Form.Item name="name_contains"
                                        >
                                            <Input placeholder="Name" />
                                        </Form.Item>
                                    </Col>
                                    <Col span={6} style={{marginLeft:"5px"}}>
                                        <Form.Item
                                            name={["user_id","partner_id","name_contains"]}
                                        >
                                            <Input placeholder="Shop name" />
                                        </Form.Item>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col span={24} style={{ textAlign: 'right',}}>
                                        <Button type="primary" htmlType="submit" className="status-btn hover-btn custom-mg-icon" icon={<SearchOutlined />}>Search</Button>
                                        <Button style={{ margin: '0 8px'}} onClick={onClear}>Clear</Button>
                                    </Col>
                                </Row>
                            </Form>
                        </div>

                        <div className="col-lg-12 col-md-12">
                            <div className="card card-static-2 mt-30 mb-30">
                                <div className="card-title-2 text-right " style={{ textAlign: 'right',}}>

                                </div>
                                <div className="card-body-table">
                                    <div className="table-responsive col-lg-12 col-md-12">
                                        <Table
                                            columns={columns}
                                            dataSource={rows}
                                            pagination={pagination}
                                            loading={loadingParent}
                                            onChange={setPagination}
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </Content>
    )
}
export default ListCensorshipProduct;
