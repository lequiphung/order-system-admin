import React, {useEffect, useState} from "react";
import Link from "next/link";
import $ from "jquery";
import {Tag, Image, Button, Modal, Spin} from 'antd';
import { SnippetsOutlined } from '@ant-design/icons';
import {useMutation} from "@apollo/client";
import {DELETE, UPDATE} from "../../../queries/product";
import Router from "next/router";
import NumberFormat from "react-number-format";
const Show = ({product}) => {

    const [product_old,setProductOld] = useState({});
    const [product_new,setProductNew] = useState(product);
    const [updateProduct, { data:update_product,loading:loading }] = useMutation(UPDATE);
    const [deleteProduct, { data:delete_product,loading:loading_delete }] = useMutation(DELETE);

    const success = () =>{
        Modal.success({
            content: `Censorship successfully`,
            onOk() {
                Router.push('/censorship/product');
            },
        });
    }

    useEffect(()=>{
        if(product.parent_id){
            setProductNew(product.parent_id);
            setProductOld(product);
        }else{
            setProductNew(product);
        }
    },[]);

    useEffect(() => {
        if(!$.isEmptyObject(update_product) && !loading){
            if(!$.isEmptyObject(product_old)){
                deleteProduct({
                    variables: {
                        id:product_old.id
                    }
                });
            }else{
                success();
            }
        }
    },[update_product,loading]);
    useEffect(() => {
        if(!$.isEmptyObject(delete_product) && !loading_delete){
            success();
        }
    },[delete_product,loading_delete]);

    let imageUrl = '';
    let product_old_imageUrl = '';
    if(!$.isEmptyObject(product_new.image)){
        imageUrl = process.env.NODE_ENV !== "development"
            ? product_new.image.url
            : process.env.API_BACKEND_URL + product_new.image.url;
    }
    if(!$.isEmptyObject(product_old.image)){
        product_old_imageUrl = process.env.NODE_ENV !== "development"
            ? product_old.image
            : process.env.API_BACKEND_URL + product_old.image.url;
    }

    const onOk = () => {

        const data = {
            parent_id:null,
            censorship:false,
            image:!$.isEmptyObject(product_new.image)?product_new.image.id:null,
            name: product_new.name,
            price:product_new.price,
            active: product_new.active,
        };
        update(data);
    };

    const onReject = () => {
        const data = {
            parent_id:null,
            censorship:false
        };
        update(data);
    }

    const update = (data) =>{
        updateProduct({
            variables:{
                input:{
                    where: { id: product.id },
                    data:data
                }
            }
        });
    }

    return (
        <div>
            <Spin spinning={loading}>
                <main>
                    <div className="container-fluid">
                        <h2 className="mt-30 page-title">Censorship</h2>
                        <ol className="breadcrumb mb-30">
                            <li className="breadcrumb-item"><a>Censorship</a></li>
                            <li className="breadcrumb-item"><Link href="/censorship/product"><a>Product</a></Link></li>
                            <li className="breadcrumb-item active">Detail</li>
                        </ol>
                        <div className="row">
                            <div className="col-lg-6 col-md-6">
                                <div className="card card-static-2 mb-30">
                                    <div className="card-body-table">
                                        <div className="shop-content-left pd-20">
                                            <div className="shop_img">
                                                <Image
                                                    height={100}
                                                    src={imageUrl}
                                                />
                                            </div>
                                            <div className="shop-dt-left right-dt">
                                                <h4 className="text-right">New <SnippetsOutlined style={{fontSize:"20px"}} /></h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="card card-static-2 mb-30">
                                    <div className="card-body-table">
                                        <div className="shopowner-content-left pd-20">
                                            <div className="shopowner-dt-left">
                                                <h4>{product_new.name}</h4>
                                            </div>
                                            <div className="shopowner-dts">
                                                <div className="shopowner-dt-list">
                                                    <span className="left-dt">Shop</span>
                                                    <span className="right-dt">{product_new.user_id?.partner_id?.name}</span>
                                                </div>
                                                <div className="shopowner-dt-list">
                                                    <span className="left-dt">Name</span>
                                                    <span className="right-dt">{product_new.name}</span>
                                                </div>
                                                <div className="shopowner-dt-list">
                                                    <span className="left-dt">price</span>
                                                    <span className="right-dt">
                                                        <NumberFormat value={product_new.price} displayType={'text'} thousandSeparator={true}/>
                                                    </span>
                                                </div>
                                                <div className="shopowner-dt-list">
                                                    <span className="left-dt">Status</span>

                                                    <span className="right-dt">
                                                    {   product_new.active?
                                                        <Tag color="green">Active</Tag>
                                                        :<Tag color="orange" >Inactive</Tag>
                                                    }
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {
                                !$.isEmptyObject(product_old)?(
                                    <div className="col-lg-6 col-md-6">
                                        <div className="card card-static-2 mb-30">
                                            <div className="card-body-table">
                                                <div className="shop-content-left pd-20">
                                                    <div className="shop_img">
                                                        <img src={product_old_imageUrl} alt="" />
                                                    </div>
                                                    <div className="shop-dt-left right-dt">
                                                        <h4 className="text-right">Old <SnippetsOutlined style={{fontSize:"20px"}} /></h4>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="card card-static-2 mb-30">
                                            <div className="card-body-table">
                                                <div className="shopowner-content-left pd-20">
                                                    <div className="shopowner-dt-left">
                                                        <h4>{product_old.name}</h4>
                                                    </div>
                                                    <div className="shopowner-dts">
                                                        <div className="shopowner-dt-list">
                                                            <span className="left-dt">Shop</span>
                                                            <span className="right-dt">{product_old.user_id?.partner_id?.name}</span>
                                                        </div>
                                                        <div className="shopowner-dt-list">
                                                            <span className="left-dt">Name</span>
                                                            <span className="right-dt">{product_old.name}</span>
                                                        </div>
                                                        <div className="shopowner-dt-list">
                                                            <span className="left-dt">price</span>
                                                            <span className="right-dt">
                                                        <NumberFormat value={product_old.price} displayType={'text'} thousandSeparator={true}/>
                                                    </span>
                                                        </div>
                                                        <div className="shopowner-dt-list">
                                                            <span className="left-dt">Status</span>

                                                            <span className="right-dt">
                                                    {   product_old.active?
                                                        <Tag color="green">Active</Tag>
                                                        :<Tag color="orange" >Inactive</Tag>
                                                    }
                                                    </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                ):''
                            }

                            <div className="col-lg-12 col-md-12 text-center mb-30">
                                <Button type="primary" onClick={onOk}>Censorship</Button>
                                <Button type="danger" onClick={onReject} style={{marginLeft:"5px"}}>Reject</Button>
                            </div>
                        </div>
                    </div>
                </main>
            </Spin>
        </div>
    );
}
export default Show