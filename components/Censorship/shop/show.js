import React, {useEffect, useState} from "react";
import Link from "next/link";
import $ from "jquery";
import {Tag, Image, Button, Modal, Spin} from 'antd';
import { SnippetsOutlined } from '@ant-design/icons';
import {useMutation} from "@apollo/client";
import {DELETE, UPDATE} from "../../../queries/partner";
import Router from "next/router";
import USER_UPDATE from "../../../queries/user/update";
const Show = ({partner ,user}) => {
    const [partner_old,setPartnerOld] = useState({});
    const [partner_new,setPartnerNew] = useState(partner);
    const [updatePartner, { data:update_partner,loading:loading }] = useMutation(UPDATE);
    const [deletePartner, { data:delete_partner,loading:loading_delete }] = useMutation(DELETE);
    const [updateUser, { data:update_user,loading:loading_user }] = useMutation(USER_UPDATE);

    const success = () =>{
        Modal.success({
            content: `Censorship successfully`,
            onOk() {
                Router.push('/censorship/shop');
            },
        });
    }

    useEffect(()=>{
        if(partner.parent_id){
            setPartnerNew(partner.parent_id);
            setPartnerOld(partner);
        }else{
            setPartnerNew(partner);
        }
    },[]);

    useEffect(() => {
        if(!$.isEmptyObject(update_partner) && !loading){
            if(!$.isEmptyObject(partner_old)){
                deletePartner({
                    variables: {
                        id:partner_old.id
                    }
                });
                updateUser({
                    variables:{
                        input:{
                            where: {
                                id: user.id
                            },
                            data:{
                                partner_id:partner_new.id
                            }
                        }
                    }
                });
            }else{
                success();
            }
        }
    },[update_partner,loading]);
    useEffect(() => {
        if(!$.isEmptyObject(delete_partner) && !loading_delete && !$.isEmptyObject(update_user) && !loading_user){
            success();
        }
    },[delete_partner,loading_delete,update_user,loading_user]);

    let imageUrl = '';
    let partner_old_imageUrl = '';
    let image_paypay_qrcode = '';
    let partner_old_image_paypay_qrcode = '';
    if(!$.isEmptyObject(partner_new.image)){
        imageUrl = process.env.NODE_ENV !== "development"
            ? partner_new.image.url
            : process.env.API_BACKEND_URL + partner_new.image.url;
    }
    if(!$.isEmptyObject(partner_new.paypay_qrcode)){
        image_paypay_qrcode = process.env.NODE_ENV !== "development"
            ? partner_new.paypay_qrcode.url
            : process.env.API_BACKEND_URL + partner_new.paypay_qrcode.url;
    }

    if(!$.isEmptyObject(partner_old.image)){
        partner_old_imageUrl = process.env.NODE_ENV !== "development"
            ? partner_old.image
            : process.env.API_BACKEND_URL + partner_old.image.url;
    }
    if(!$.isEmptyObject(partner_old.paypay_qrcode)){
        partner_old_image_paypay_qrcode = process.env.NODE_ENV !== "development"
            ? partner_old.paypay_qrcode.url
            : process.env.API_BACKEND_URL + partner_old.paypay_qrcode.url;
    }

    const onOk = () => {

        const data = {
            parent_id:null,
            censorship:false,
            image:!$.isEmptyObject(partner_new.image)?partner_new.image.id:null,
            image_description:!$.isEmptyObject(partner_new.image_description)?partner_new.image_description.id:null,
            paypay_qrcode:!$.isEmptyObject(partner_new.paypay_qrcode)?partner_new.paypay_qrcode.id:null,
            active: partner_new.active,
            name: partner_new.name,
            phone: partner_new.phone,
            address:partner_new.address,
            latitude: partner_new.latitude,
            longitude: partner_new.longitude,
            open_hours: partner_new.open_hours,
            close_hours: partner_new.close_hours,
        };
        update(data);
    };

    const onReject = () => {
        const data = {
            parent_id:null,
            censorship:false
        };
        update(data);
    }

    const update = (data) =>{
        updatePartner({
            variables:{
                input:{
                    where: { id: partner.id },
                    data:data
                }
            }
        });
    }

    return (
        <div>
            <Spin spinning={loading}>
                <main>
                <div className="container-fluid">
                    <h2 className="mt-30 page-title">Censorship</h2>
                    <ol className="breadcrumb mb-30">
                        <li className="breadcrumb-item"><a>Censorship</a></li>
                        <li className="breadcrumb-item"><Link href="/censorship/shop"><a>Shop</a></Link></li>
                        <li className="breadcrumb-item active">Detail</li>
                    </ol>
                    <div className="row">
                        <div className="col-lg-6 col-md-6">
                            <div className="card card-static-2 mb-30">
                                <div className="card-body-table">
                                    <div className="shop-content-left pd-20">
                                        <div className="shop_img">
                                            <Image
                                                height={100}
                                                src={imageUrl}
                                            />
                                        </div>
                                        <div className="shop-dt-left right-dt">
                                            <h4 className="text-right">New <SnippetsOutlined style={{fontSize:"20px"}} /></h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="card card-static-2 mb-30">
                                <div className="card-body-table">
                                    <div className="shopowner-content-left pd-20">
                                        <div className="shopowner-dt-left">
                                            <h4>{partner_new.name}</h4>
                                        </div>
                                        <div className="shopowner-dts">
                                            <div className="shopowner-dt-list">
                                                <span className="left-dt">Username</span>
                                                <span className="right-dt">{user.username}</span>
                                            </div>
                                            <div className="shopowner-dt-list">
                                                <span className="left-dt">Name</span>
                                                <span className="right-dt">{partner_new.name}</span>
                                            </div>
                                            <div className="shopowner-dt-list">
                                                <span className="left-dt">Phone</span>
                                                <span className="right-dt">{partner_new.phone}</span>
                                            </div>
                                            <div className="shopowner-dt-list">
                                                <span className="left-dt">Email</span>
                                                <span className="right-dt">{user.email}</span>
                                            </div>
                                            <div className="shopowner-dt-list">
                                                <span className="left-dt">Address</span>
                                                <span className="right-dt">{partner_new.address}</span>
                                            </div>

                                            <div className="shopowner-dt-list">
                                                <span className="left-dt">Open hours</span>
                                                <span className="right-dt">
                                                    {partner_new.open_hours}
                                                </span>
                                            </div>
                                            <div className="shopowner-dt-list">
                                                <span className="left-dt">Close hours</span>
                                                <span className="right-dt">
                                                    {partner_new.close_hours}
                                                </span>
                                            </div>
                                            <div className="shopowner-dt-list">
                                                <span className="left-dt">Latitude</span>
                                                <span className="right-dt">{partner_new.latitude}</span>
                                            </div>
                                            <div className="shopowner-dt-list">
                                                <span className="left-dt">Longitude</span>
                                                <span className="right-dt">{partner_new.longitude}</span>
                                            </div>
                                            <div className="shopowner-dt-list">
                                                <span className="left-dt">Status</span>

                                                <span className="right-dt">
                                                    {   partner_new.active?
                                                        <Tag color="green">Active</Tag>
                                                        :<Tag color="orange" >Inactive</Tag>
                                                    }
                                                </span>
                                            </div>

                                            <div className="shopowner-dt-list">
                                                <span className="left-dt">Paypay_qrcode</span>
                                                <span className="right-dt">
                                                    <Image
                                                        width={100}
                                                        src={image_paypay_qrcode}
                                                    />
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {
                            !$.isEmptyObject(partner_old)?(
                                <div className="col-lg-6 col-md-6">
                                    <div className="card card-static-2 mb-30">
                                        <div className="card-body-table">
                                            <div className="shop-content-left pd-20">
                                                <div className="shop_img">
                                                    <img src={partner_old_imageUrl} alt="" />
                                                </div>
                                                <div className="shop-dt-left right-dt">
                                                    <h4 className="text-right">Old <SnippetsOutlined style={{fontSize:"20px"}} /></h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="card card-static-2 mb-30">
                                        <div className="card-body-table">
                                            <div className="shopowner-content-left pd-20">
                                                <div className="shopowner-dt-left">
                                                    <h4>{partner_old.name}</h4>
                                                </div>
                                                <div className="shopowner-dts">
                                                    <div className="shopowner-dt-list">
                                                        <span className="left-dt">Username</span>
                                                        <span className="right-dt">{user.username}</span>
                                                    </div>
                                                    <div className="shopowner-dt-list">
                                                        <span className="left-dt">Name</span>
                                                        <span className="right-dt">{partner_old.name}</span>
                                                    </div>
                                                    <div className="shopowner-dt-list">
                                                        <span className="left-dt">Phone</span>
                                                        <span className="right-dt">{partner_old.phone}</span>
                                                    </div>
                                                    <div className="shopowner-dt-list">
                                                        <span className="left-dt">Email</span>
                                                        <span className="right-dt">{user.email}</span>
                                                    </div>
                                                    <div className="shopowner-dt-list">
                                                        <span className="left-dt">Address</span>
                                                        <span className="right-dt">{partner_old.address}</span>
                                                    </div>

                                                    <div className="shopowner-dt-list">
                                                        <span className="left-dt">Open hours</span>
                                                        <span className="right-dt">{partner_old.open_hours}</span>
                                                    </div>
                                                    <div className="shopowner-dt-list">
                                                        <span className="left-dt">Close hours</span>
                                                        <span className="right-dt">{partner_old.close_hours}</span>
                                                    </div>
                                                    <div className="shopowner-dt-list">
                                                        <span className="left-dt">Latitude</span>
                                                        <span className="right-dt">{partner_old.latitude}</span>
                                                    </div>
                                                    <div className="shopowner-dt-list">
                                                        <span className="left-dt">Longitude</span>
                                                        <span className="right-dt">{partner_old.longitude}</span>
                                                    </div>
                                                    <div className="shopowner-dt-list">
                                                        <span className="left-dt">Status</span>

                                                        <span className="right-dt">
                                                            {   partner.active?
                                                                <Tag color="green">Active</Tag>
                                                                :<Tag color="orange" >Inactive</Tag>
                                                            }
                                                        </span>
                                                    </div>
                                                    <div className="shopowner-dt-list">
                                                        <span className="left-dt">Paypay_qrcode</span>
                                                        <span className="right-dt">
                                                    <Image
                                                        width={100}
                                                        src={partner_old_image_paypay_qrcode}
                                                    />
                                                </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            ):''
                        }

                        <div className="col-lg-12 col-md-12 text-center mb-30">
                            <Button type="primary" onClick={onOk}>Censorship</Button>
                            <Button type="danger" onClick={onReject} style={{marginLeft:"5px"}}>Reject</Button>
                        </div>
                    </div>
                </div>
            </main>
            </Spin>
        </div>
    );
}
export default Show