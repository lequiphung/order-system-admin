import {Button, Col, Form, Input, Layout, Row, Spin, Switch,TimePicker} from 'antd';
import Link from "next/link";
import UploadImage from "../Partner/upload-image";
import Wrapper from "../Wrapper";
import React from "react";
import { ArrowLeftOutlined } from '@ant-design/icons';
import {useSelector} from "react-redux";
import moment from "moment";
const { Content } = Layout;
const UpdateFormCustomer = ({ object,onFinish,loading,setImage }) => {
    const {object_info} = useSelector(state => state.partnerReducer);
    const [form] = Form.useForm();

    let initialValues = {};
    if(object){
        initialValues = {
            name:object.partner_id.name,
            phone:object.partner_id.phone,
            address:object.partner_id.address,
            active:object.partner_id.active,
            user: {
                username:object.username,
                email:object.email,
                confirmed:object.confirmed,
            },
        }
    }

    const onClear = () =>{
        form.resetFields();
    }
    return (
        <Content>

            <Spin spinning={loading}>

                <main>
                    <div className="container-fluid">
                        <h2 className="mt-30 page-title">{object_info.title}</h2>
                        <ol className="breadcrumb mb-30">
                            <li className="breadcrumb-item"><Link href={object_info.link_back}><a>{object_info.title}</a></Link></li>
                            <li className="breadcrumb-item active">{object_info.action}</li>

                        </ol>
                        <div className="row">
                            <div className="col-lg-12 col-md-12 mt-30">
                                <Col>
                                    <Link href={object_info.link_back}>
                                        <Button type="danger" className="custom-mg-icon" icon={<ArrowLeftOutlined />} >
                                            Back
                                        </Button>
                                    </Link>
                                </Col>
                            </div>
                            <div className="col-lg-12">
                                <div className="add-new-shop">
                                    <div className="card card-static-2 mb-30">
                                        <Form
                                            form={form}
                                            name="advanced_search"
                                            className="ant-advanced-search-form"
                                            onFinish={onFinish}
                                            initialValues={initialValues}
                                        >
                                            <div className="row no-gutters">

                                                <div className="col-lg-12 col-md-12">
                                                    <div className="card-body-table"  style={{ "paddingTop": '20px',}}>
                                                        <div className="add-shop-content pd-20 row">

                                                            <div className="col-lg-6 col-md-6">
                                                                <div className="form-group">
                                                                    <label className="form-label">Username <span className={'required'}>*</span></label>
                                                                    <Form.Item name={['user', 'username']} rules={[
                                                                        {
                                                                            required: true,
                                                                            message: 'Please input username!',
                                                                        },
                                                                    ]}>
                                                                        <Input type="text" className="form-control" placeholder="Username"/>
                                                                    </Form.Item>
                                                                </div>
                                                            </div>
                                                            <div className="col-lg-6 col-md-6">
                                                                <div className="form-group">
                                                                    <label className="form-label">Password {!object && <span className={'required'}>*</span>}</label>
                                                                    <Form.Item name={['user', 'password']} rules={!object && [
                                                                        {
                                                                            required: true,
                                                                            message: 'Please input Password!',
                                                                        },
                                                                    ]}>
                                                                        <Input type="password" className="form-control" placeholder="Password" />
                                                                    </Form.Item>
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <div className="add-shop-content pd-20 row">

                                                            <div className="col-lg-6 col-md-6">

                                                                <div className="form-group">
                                                                    <label className="form-label">Name <span className={'required'}>*</span></label>
                                                                    <Form.Item name="name" rules={[
                                                                        {
                                                                            required: true,
                                                                            message: 'Please input Name!',
                                                                        },
                                                                    ]}>
                                                                        <Input type="text" className="form-control" placeholder="Name"/>
                                                                    </Form.Item>
                                                                </div>
                                                                <div className="form-group">
                                                                    <label className="form-label">Email <span className={'required'}>*</span></label>
                                                                    <Form.Item name={['user', 'email']} rules={[
                                                                        {
                                                                            required: true,
                                                                            message: 'Please input email!',
                                                                        },
                                                                        {
                                                                            type: 'email',
                                                                            message: 'Email is not validate email!',
                                                                        },
                                                                    ]}>
                                                                        <Input type="text" className="form-control" placeholder="Email"/>
                                                                    </Form.Item>
                                                                </div>


                                                            </div>
                                                            <div className="col-lg-6 col-md-6">
                                                                <div className="form-group">
                                                                    <label className="form-label">Phone number <span className={'required'}>*</span></label>
                                                                    <Form.Item name="phone" rules={[
                                                                        {
                                                                            required: true,
                                                                            message: 'Please input phone number',
                                                                        },
                                                                    ]}>
                                                                        <Input type="text" className="form-control" placeholder="Phone number"/>
                                                                    </Form.Item>
                                                                </div>

                                                                <div className="form-group">
                                                                    <label className="form-label">Address</label>
                                                                    <Form.Item name="address">
                                                                        <Input type="text" className="form-control" placeholder="Address"/>
                                                                    </Form.Item>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div className="add-shop-content pd-20">
                                                            <div className="col-lg-12 col-md-12">
                                                                <div className="form-group">
                                                                    <label className="form-label">Image</label>
                                                                    <UploadImage setImage={setImage} image={object && object.partner_id.image?object.partner_id.image:false}/>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div className="add-shop-content pd-20">
                                                            <div className="col-lg-12 col-md-12">
                                                                <Row gutter={3}>
                                                                    <Form.Item name={['user', 'confirmed']} label="Confirmed" valuePropName="checked">
                                                                        <Switch  defaultChecked={object && object.confirmed?true:false}/>
                                                                    </Form.Item>
                                                                    <Form.Item name="active" label="Status" valuePropName="checked" className="ml-30">
                                                                        <Switch defaultChecked={object && object.partner_id.active?true:false}/>
                                                                    </Form.Item>
                                                                </Row>
                                                            </div>
                                                        </div>

                                                        <div className="add-shop-content pd-20">
                                                            <div className="col-lg-12 col-md-12">
                                                                <Col span={24} style={{ textAlign: 'right',}}>
                                                                    <Button type="danger" htmlType="submit" className="status-btn hover-btn"> Submit</Button>
                                                                    <Button style={{ margin: '0 8px'}} onClick={onClear}>Reset</Button>
                                                                </Col>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </Form>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>

            </Spin>
        </Content>
    )
}
export default UpdateFormCustomer
