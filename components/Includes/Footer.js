import React from "react";

export default function Footer() {
    return (
        <div>
            <footer className="main-footer">
                <strong>Copyright &copy; 2020-2021 <a href="/">System Order</a>.</strong>
                All rights reserved.
            </footer>
        </div>
    )
}