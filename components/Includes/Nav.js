import React from "react";
import Link from 'next/link';
import { useRouter } from 'next/router'

export default function Nav() {
    const router = useRouter();
    return (
        <div id="layoutSidenav_nav">
            <nav className="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                <div className="sb-sidenav-menu">
                    <div className="nav">
                        <Link href="/">
                            <a className="nav-link">
                                <div className="sb-nav-link-icon"><i className="fas fa-tachometer-alt"></i></div>
                                Home
                            </a>
                        </Link>



                        <a className={router.pathname.indexOf('/shop') == 0?'active nav-link':'collapsed nav-link'} href="#" data-toggle="collapse" data-target="#collapseShops" aria-expanded="false" aria-controls="collapseShops">
                            <div className="sb-nav-link-icon"><i className="fas fa-store"></i></div>
                            Shops
                            <div className="sb-sidenav-collapse-arrow"><i className="fas fa-angle-down"></i>
                            </div>
                        </a>
                        <div className={router.pathname.indexOf('/shop') == 0?'collapse show':'collapse'} id="collapseShops" aria-labelledby="headingTwo"
                             data-parent="#sidenavAccordion">
                            <nav className="sb-sidenav-menu-nested nav">
                                <Link href={"/shop"}>
                                    <a className={router.pathname.indexOf('/shop') == 0 && router.pathname.indexOf('create') == -1?'nav-link sub_nav_link active':'nav-link sub_nav_link'}>All Shops</a>
                                </Link>
                                <Link href={"/shop/create"}>
                                    <a className={router.pathname.indexOf('/shop/create') == 0?'nav-link sub_nav_link active':'nav-link sub_nav_link'}>Add Shop</a>
                                </Link>

                            </nav>
                        </div>

                        <a className={router.pathname.indexOf('/customer') == 0?'active nav-link':'collapsed nav-link'} href="#" data-toggle="collapse" data-target="#collapseCustomer" aria-expanded="false" aria-controls="collapseCustomer">
                            <div className="sb-nav-link-icon"><i className="fas fa-users"></i></div>
                            Customers
                            <div className="sb-sidenav-collapse-arrow"><i className="fas fa-angle-down"></i>
                            </div>
                        </a>
                        <div className={router.pathname.indexOf('/customer') == 0?'collapse show':'collapse'} id="collapseCustomer" aria-labelledby="headingTwo"
                             data-parent="#sidenavAccordion">
                            <nav className="sb-sidenav-menu-nested nav">
                                <Link href={"/customer"}>
                                    <a className={router.pathname.indexOf('/customer') == 0 && router.pathname.indexOf('create') == -1?'nav-link sub_nav_link active':'nav-link sub_nav_link'}>All Customers</a>
                                </Link>
                                <Link href={"/customer/create"}>
                                    <a className={router.pathname.indexOf('/customer/create') == 0?'nav-link sub_nav_link active':'nav-link sub_nav_link'}>Add Customer</a>
                                </Link>

                            </nav>
                        </div>


                        <a className={router.pathname.indexOf('/censorship') == 0?'active nav-link':'collapsed nav-link'} href="#" data-toggle="collapse" data-target="#collapseCensorship" aria-expanded="false" aria-controls="collapseCensorship">
                            <div className="sb-nav-link-icon"><i className="fas fa-users"></i></div>
                            Censorship
                            <div className="sb-sidenav-collapse-arrow"><i className="fas fa-angle-down"></i>
                            </div>
                        </a>
                        <div className={router.pathname.indexOf('/censorship') == 0?'collapse show':'collapse'} id="collapseCensorship" aria-labelledby="headingTwo"
                             data-parent="#sidenavAccordion">
                            <nav className="sb-sidenav-menu-nested nav">
                                <Link href={"/censorship/shop"}>
                                    <a className={router.pathname.indexOf('/censorship/shop') == 0?'nav-link sub_nav_link active':'nav-link sub_nav_link'}>Shop</a>
                                </Link>
                                <Link href={"/censorship/product"}>
                                    <a className={router.pathname.indexOf('/censorship/product') == 0?'nav-link sub_nav_link active':'nav-link sub_nav_link'}>Product</a>
                                </Link>

                            </nav>
                        </div>
                    </div>
                </div>
            </nav>
        </div>
    )
}