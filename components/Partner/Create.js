import React, {useEffect, useState} from "react";
import { Modal } from 'antd';
import $ from "jquery";
import {formatDataForm,failNotification} from "../../function";
import moment from 'moment';
import {useLazyQuery, useMutation} from "@apollo/client";
import USER_CREATE from "../../queries/user/create";
import USER_LIST from "../../queries/user/find";
import Router from "next/router";
import UpdateForm from "./update-form";
import {CREATE, LIST} from "../../queries/partner";
import UpdateFormCustomer from "../Customer/update-form";
import {useSelector} from "react-redux";
import cloneDeep from 'lodash/cloneDeep';
const format = 'HH:mm';

const PartnerCreate = () => {
    const {object_info} = useSelector(state => state.partnerReducer);
    const [data,setData] = useState({});
    const [image,setImage] = useState('');
    const [paypay_qrcode,setPaypayQrcode] = useState('');
    const [checking,setChecking] = useState(false);
    const [submit,setSubmit] = useState(false);
    const [loading,setLoading] = useState(false);

    const [getUsers, { called:called_user, loading:loading_users, data:users}] = useLazyQuery(USER_LIST);
    const [createPartner, { data:create_partner,loading:loading_partner }] = useMutation(CREATE);
    const [createUser, { data:create_user,loading:loading_user }] = useMutation(USER_CREATE);

    useEffect(() => {
        if(loading_users || loading_partner || loading_user){
            setLoading(true);
        }else{
            setLoading(false);
        }
    },[loading_users,loading_partner,loading_user]);

    useEffect(() => {
        if(users !=  undefined && !loading_users){
            console.log(users.users);
            if(users.users.length){
                failNotification('User already exists!')
                setSubmit(false);
            }else{
                setSubmit(true);
            }
            setChecking(false);
        }

    },[loading_users,users]);

    useEffect(() => {
        if(!$.isEmptyObject(create_partner) && !loading_partner){
            console.log(create_partner);

            const data_user = data.user;
            console.log(data_user);
            data_user["partner_id"] = create_partner.createPartner.partner.id;
            console.log(data_user);
            createUser({
                variables:{
                    input:{
                        data:data_user
                    }
                }
            });

        }
    },[create_partner,loading_partner]);

    useEffect(() => {
        if(!$.isEmptyObject(create_user) && !loading_user){
            Modal.success({
                content: `Create ${object_info.title} successfully`,
                onOk() {
                    Router.push(`${object_info.link_back}`);
                },
            });
        }
    },[create_user,loading_user]);

    useEffect(() => {
        if(!checking && submit){
            const data_partner = cloneDeep(data);
            //  data_partner["user_id"] = create_user.createUser.user.id;
             data_partner["supplier"] = object_info.shop;
             data_partner["customer"] = object_info.customer;
            delete data_partner.user;
            console.log(data,data_partner);
            console.log(data.user);
            createPartner({
                variables:{
                    input:{
                        data:data_partner
                    }
                }
            });

        }
    },[submit]);

    const checkUser = (data) => {
        setChecking(true);
        getUsers({
            variables:{
                where:{
                    _or:[{username:data.username},{email:data.email}]
                }
            },
            fetchPolicy: 'network-only'
        });
    }

    const onFinish = values => {
        const data_form = formatDataForm(values);
        if(data_form.open_hours){
            data_form.open_hours = moment(data_form.open_hours).format(format);
        }
        if(data_form.close_hours){
            data_form.close_hours = moment(data_form.close_hours).format(format);
        }
        if(data_form.longitude){
            data_form.longitude = parseFloat(data_form.longitude);
        }
        if(data_form.latitude){
            data_form.latitude = parseFloat(data_form.latitude);
        }

        setData(data => ({...data_form,image:image,paypay_qrcode:paypay_qrcode}));
        checkUser(data_form.user);
    };

    if(object_info.customer){
        return <UpdateFormCustomer object={false} onFinish={onFinish} loading={loading} setImage={setImage}/>
    }

    return <UpdateForm object={false} onFinish={onFinish} loading={loading} setImage={setImage} setPaypayQrcode={setPaypayQrcode} object_info={object_info}/>
}
export default PartnerCreate;