import React, {useEffect, useState} from "react";
import Wrapper from "../Wrapper";
import {Layout, Modal} from 'antd';
import $ from "jquery";
import {formatDataForm,failNotification} from "../../function";
import moment from 'moment';
import {useLazyQuery, useMutation, useQuery} from "@apollo/client";
import Router, {useRouter} from "next/router";
import USER_UPDATE from "../../queries/user/update";
import UpdateForm from "./update-form";
import USER_LIST from "../../queries/user/find";
import {UPDATE} from "../../queries/partner";
import {FIND} from "../../queries/user";
import UpdateFormCustomer from "../Customer/update-form";
import {useSelector} from "react-redux";
import cloneDeep from "lodash/cloneDeep";

const { Content } = Layout;
const format = 'HH:mm';

const PartnerEdit = () => {
    const {object_info} = useSelector(state => state.partnerReducer);
    const router = useRouter()
    const { id } = router.query
    const { loading:loading_find, error, data:partner } = useQuery(FIND, {
        variables: {id:id},fetchPolicy: 'network-only'
    });
    const [object,setObject] = useState(false);
    const [data,setData]    = useState({});
    const [image,setImage]  = useState('');
    const [paypay_qrcode,setPaypayQrcode] = useState('');
    const [checking,setChecking] = useState(false);
    const [submit,setSubmit] = useState(false);
    const [loading,setLoading] = useState(false);

    const [getUsers, { called:called_user, loading:loading_users, data:users}] = useLazyQuery(USER_LIST);

    const [updatePartner, { data:update_partner,loading:loading_partner }] = useMutation(UPDATE);

    const [updateUser, { data:update_user,loading:loading_user }] = useMutation(USER_UPDATE);

    useEffect(() => {
        if(partner !=  undefined && !loading_find){
            setObject(partner.user);
        }
    },[partner,loading_find]);

    useEffect(() => {
        if(loading_users || loading_partner || loading_user){
            setLoading(true);
        }else{
            setLoading(false);
        }
    },[loading_users,loading_partner,loading_user]);

    useEffect(() => {
        if(users !=  undefined && !loading_users){
            console.log(users.users);
            if(users.users.length){
                failNotification('User already exists!')
                setSubmit(false);
            }else{
                setSubmit(true);
            }
            setChecking(false);
        }

    },[loading_users,users]);

    useEffect(() => {
        if(!$.isEmptyObject(update_partner) && !loading_partner){
            Modal.success({
                content: `Update ${object_info.title} successfully`,
                onOk() {
                    Router.push(`${object_info.link_back}`);
                },
            });
        }
    },[update_partner,loading_partner]);

    useEffect(() => {
        if(!$.isEmptyObject(update_user) && !loading_user){
            const data_shop = cloneDeep(data);
            delete data_shop["user"];
            updatePartner({
                variables:{
                    input:{
                        where: { id: object.partner_id.id },
                        data:data_shop
                    }
                }
            });
        }
    },[update_user,loading_user]);

    useEffect(() => {
        if(!checking && submit){
            updateUser({
                variables:{
                    input:{
                        where: {
                            id: id
                        },
                        data:data.user
                    }
                }
            });
        }

    },[submit]);

    const checkUser = (data) => {
        setChecking(true);
        getUsers({
            variables:{
                where:{
                    _or:[{username:data.username},{email:data.email}],
                    id_ne:id
                }
            },
            fetchPolicy: 'network-only'
        });
    }

    const onFinish = values => {
        const data_form = formatDataForm(values);
        if(object_info.shop){
            if(data_form.open_hours){
                data_form.open_hours = moment(data_form.open_hours).format(format);
            }else{
                data_form.open_hours = '';
            }
            if(data_form.close_hours){
                data_form.close_hours = moment(data_form.close_hours).format(format);
            }else{
                data_form.close_hours = '';
            }
            if(data_form.longitude){
                data_form.longitude = parseFloat(data_form.longitude);
            }else{
                data_form.longitude = 0;
            }
            if(data_form.latitude){
                data_form.latitude = parseFloat(data_form.latitude);
            }else{
                data_form.latitude = 0;
            }
        }

        setData(data => ({...data_form,image:image,paypay_qrcode:paypay_qrcode}));
        checkUser(data_form.user);
    };

    if(!object){
        return (
            <Content>
                Data not found!
            </Content>
        )
    }

    if(object_info.customer){
        return <UpdateFormCustomer object={object} onFinish={onFinish} loading={loading} setImage={setImage}/>
    }

    return <UpdateForm object={object} onFinish={onFinish} loading={loading} setImage={setImage} setPaypayQrcode={setPaypayQrcode}/>
}

export default PartnerEdit