import Wrapper from "../Wrapper";
import {Button, Col, Form, Input, notification, Row, Select, Table, Tag, Layout, Modal} from "antd";
import Link from "next/link";
import React, {useEffect, useState} from "react";
import $ from "jquery";
import {useMutation} from "@apollo/client";
import {DELETE} from "../../queries/partner";
import {DELETE as DELETE_USER} from "../../queries/user";
import { PlusCircleOutlined,SearchOutlined,ExclamationCircleOutlined } from '@ant-design/icons';
import {successNotification} from "../../function";

const { Content } = Layout;
const { Option } = Select;
const { confirm } = Modal;
const PartnerList = (
    {
        partners,
        pagination,
        setPagination,
        loadingParent,
        setReload,
        where_default,
        setWhere,
        object_info
    }) => {

    if(!partners){
        return <div></div>;
    }
    const where_fields = {
        "username_contains" : 'Username',
        "partner_id[name_contains]":'Name',
        "partner_id[phone_contains]":'Phone',
    }
    const [form] = Form.useForm();

    const onFinish = values => {
        let new_where = where_default;
        Object.entries(values).forEach(entry => {
            let [key, value] = entry;
            if(value!= undefined && value != ""){
                let index_sub = key.indexOf("[");
                if(index_sub > 0){
                    let child = key.substring(index_sub + 1,key.indexOf("]"));
                    key = key.replace(/\[.*\]/,'');
                    let tmp = {};
                    tmp[child] = value;
                    new_where[key] = tmp;
                }else{
                    new_where[key] = value;
                }
            }
        });
        setWhere(value => ({...where_default,...new_where}));
    };

    const onClear = () =>{
        form.resetFields();
        setWhere(where_default);
    }

    const getFields = () => {
        const children = [];

        Object.entries(where_fields).forEach(entry => {
            const [key, label] = entry;
            children.push(
                <Col span={6} key={key}>
                    <Form.Item
                        name={key}
                    >
                        <Input placeholder={label} />
                    </Form.Item>
                </Col>,
            );
        });
        return children;
    };

    let rows = [];
    partners.users.map((item,i) => {
        let imageUrl = '';
        if(!$.isEmptyObject(item.partner_id.image)){
            imageUrl = process.env.NODE_ENV !== "development"
                ? item.partner_id.image.url
                : process.env.API_BACKEND_URL + item.partner_id.image.url;
        }
        rows.push(
            {
                id:item.id,
                key: item.id,
                index:i+1,
                username: item.username,
                name: item.partner_id?.name,
                image: imageUrl,
                phone: item.partner_id?.phone,
                active: item.partner_id?.active,
                partner_id:item.partner_id?.id,
            }
        );
        return rows
    });

    const [loadingList,setLoadingList] = useState(false);
    const [deletePartner, { data,loading }] = useMutation(DELETE);
    const [deleteUser, { data:data_user,loading:loading_user }] = useMutation(DELETE_USER);

    const showDeleteConfirm = (id,partner_id) =>{
        confirm({
            title: 'Are you sure delete this record?',
            icon: <ExclamationCircleOutlined />,
            //content: 'Some descriptions',
            okText: 'Yes',
            okType: 'danger',
            cancelText: 'No',
            onOk() {
                deletePartner({
                    variables: {
                        id:partner_id
                    }
                });
                deleteUser({
                    variables: {
                        id:id
                    }
                });
            },
            onCancel() {
                console.log('Cancel');
                setReload(true);
            },
        });
    }

    useEffect(() => {
        if(!$.isEmptyObject(data) && !loading && !$.isEmptyObject(data_user) && !loading_user){
            successNotification('Delete successfully');
            setReload(true);
        }
    },[data,loading,data_user,loading_user]);

    useEffect(() => {
        if(loadingParent || loading || loading_user){
            setLoadingList(true);
        }else{
            setLoadingList(false);
        }
    },[loadingParent,loading, loading_user]);

    const columns = [
        {
            title: '#',
            dataIndex: 'index',
            width: 150,
        },
        {
            title: 'Username',
            dataIndex: 'username',
            width: 270,
        },
        {
            title: 'Name',
            dataIndex: 'name',
            width: 270,
        },
        {
            title: 'Image',
            dataIndex: 'image',
            width: 200,
            render: image => <img alt={image} src={image} height="50" />
        },
        {
            title: 'Phone',
            dataIndex: 'phone',
            width: 100,
        },
        {
            title: 'Status',
            dataIndex: 'active',
            width: 150,
            render: (text, record) => {
                return text?<Tag color="green">Active</Tag>:<Tag color="red">Inactive</Tag>;
            },
        },
        {
            title: 'Action',
            key: 'operation',
            fixed: 'right',
            width: 100,
            render: (text, record) => (
                <div className="action-btns">
                    <a onClick={() => showDeleteConfirm(record.id,record.partner_id)}><i className="fa fa-trash"></i></a>
                    <Link href={`${object_info.edit_prefix}[id]/edit`} as={`${object_info.edit_prefix}${record.id}/edit`}>
                        <a className="edit-btn"><i className="fas fa-edit"></i></a>
                    </Link>
                </div>
            ),
        },
    ];

    return (

            <Content>
                <main>
                    <div className="container-fluid">
                        <h2 className="mt-30 page-title">{object_info.title}</h2>
                        <ol className="breadcrumb mb-30">
                            <li className="breadcrumb-item">{object_info.title}</li>
                            <li className="breadcrumb-item active">List</li>
                        </ol>
                        <div className="row justify-content-between">

                            <div className="col-lg-12 col-md-12 mt-30">
                                <Form
                                    form={form}
                                    name="advanced_search"
                                    className="ant-advanced-search-form"
                                    onFinish={onFinish}
                                >
                                    <Row>
                                        <Col span={6}>
                                            <Form.Item
                                                name="active"
                                            >
                                                <Select
                                                    showSearch
                                                    placeholder="Select a status"
                                                    // optionFilterProp="children"
                                                    filterOption={(input, option) =>
                                                        option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                                    }
                                                >
                                                    <Option value="">All</Option>
                                                    <Option value="1">Active</Option>
                                                    <Option value="0">Inactive</Option>
                                                </Select>
                                            </Form.Item>
                                        </Col>
                                    </Row>
                                    <Row gutter={2}>{getFields()}

                                    </Row>
                                    <Row>
                                        <Col span={24} style={{ textAlign: 'right',}}>
                                            <Button type="primary" htmlType="submit" className="status-btn hover-btn custom-mg-icon" icon={<SearchOutlined />}>Search</Button>
                                            <Button style={{ margin: '0 8px'}} onClick={onClear}>Clear</Button>
                                        </Col>
                                    </Row>
                                </Form>
                            </div>
                            <div className="col-lg-12 col-md-12 mt-30">
                                <Col>
                                    <Link href={object_info.create_link}>
                                        <Button  type="primary" style={{ background: "#87d068", borderColor: "#87d068"}} className="custom-mg-icon" icon={<PlusCircleOutlined />} >
                                            Add new
                                        </Button>
                                    </Link>
                                </Col>
                            </div>

                            <div className="col-lg-12 col-md-12">
                                <div className="card card-static-2 mb-30">
                                    <div className="card-title-2 text-right " style={{ textAlign: 'right',}}>

                                    </div>
                                    <div className="card-body-table">
                                        <div className="table-responsive col-lg-12 col-md-12">
                                            <Table
                                                columns={columns}
                                                dataSource={rows}
                                                pagination={pagination}
                                                loading={loadingList}
                                                onChange={setPagination}
                                            />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </Content>

    )
}
export default PartnerList