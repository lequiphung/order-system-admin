import React from 'react';
import {Upload, Modal, notification} from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import axios from "axios";
import {failNotification,successNotification} from "../../function";
import $ from "jquery";

function getBase64(file) {
    return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => resolve(reader.result);
        reader.onerror = error => reject(error);
    });
}

class UploadQrcode extends React.Component {

    state = {
        previewVisible: false,
        previewImage: '',
        previewTitle: '',
        fileList: [],
    };

    componentDidMount() {
        if(this.props.image){
            const image = this.props.image;
            let imageUrl = '';
            if(!$.isEmptyObject(image)){
                this.props.setPaypayQrcode(image.id);
                imageUrl = process.env.NODE_ENV !== "development"
                    ? image.url
                    : process.env.API_BACKEND_URL + image.url;
                this.setState(state => ({
                    fileList: [{
                        uid: image.id,
                        name: image.name,
                        status: 'done',
                        url: imageUrl,
                    }],
                }));
            }
        }
    }

    beforeUpload = file => {
        this.setState(state => ({
            fileList: [...state.fileList, file],
        }));
        return false;
    }

    handleCancel = () => this.setState({ previewVisible: false });

    handlePreview = async file => {
        if (!file.url && !file.preview) {
            file.preview = await getBase64(file.originFileObj);
        }

        this.setState({
            previewImage: file.url || file.preview,
            previewVisible: true,
            previewTitle: file.name || file.url.substring(file.url.lastIndexOf('/') + 1),
        });
    };

    handleChange = ({ fileList }) => {
        this.setState({ fileList });
        this.props.setPaypayQrcode('');
        if(fileList.length < 1){
            return false;
        }
        let formData = new FormData();
        formData.append('files', fileList[0].originFileObj);

        const api_upload = process.env.API_BACKEND_URL+'/upload';
        axios.post(api_upload,formData).then(res => {
            successNotification('upload successfully.');
            this.props.setPaypayQrcode(res.data[0]._id);
        }).catch(res => {
            failNotification('upload failed.');
            this.props.setPaypayQrcode('');
        });
    }

    render() {
        const { previewVisible, previewImage, fileList, previewTitle } = this.state;
        const uploadButton = (
            <div>
                <PlusOutlined />
                <div className="ant-upload-text">Upload</div>
            </div>
        );
        return (
            <div className="clearfix">
                <Upload
                    listType="picture-card"
                    fileList={fileList}
                    beforeUpload={this.beforeUpload}
                    onPreview={this.handlePreview}
                    onChange={this.handleChange}
                >
                    {fileList.length >= 1 ? null : uploadButton}
                </Upload>
                <Modal
                    visible={previewVisible}
                    title={previewTitle}
                    footer={null}
                    onCancel={this.handleCancel}
                >
                    <img alt="example" style={{ width: '100%' }} src={previewImage} />
                </Modal>
            </div>
        );
    }
}
export default UploadQrcode