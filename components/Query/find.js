import React, {useState} from "react";
import {useQuery} from "@apollo/client";

const QueryFind = ({ children, query, id}) => {
    const { data, loading, error } = useQuery(query, {
        variables: { id: id}
    });

    if (loading) return <p>Loading...</p>;
    if (error) return <p>Error: {JSON.stringify(error)}</p>;
    return children({ data });
};

export default QueryFind;
