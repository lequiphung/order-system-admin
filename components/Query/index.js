import React, {useState} from "react";
import {useQuery} from "@apollo/client";

const Query = ({ children, query, id, params,setPagination }) => {
    // const [pagination, setPagination] = useState({
    //     current: 1,
    //     pageSize: 1,
    //     total:10
    // });
    // console.log(pagination);

    let limit = 10;
    let start = 0;
    let where = {};
    if(params && params.limit){
        limit = params.limit;
    }
    if(params && params.start){
        start = params.start;
    }

    const { data, loading, error } = useQuery(query, {
        variables: { id: id,limit:limit,start:start,where:where}
    });

    if (loading) return <p>Loading...</p>;
    if (error) return <p>Error: {JSON.stringify(error)}</p>;
    return children({ data });
};

export default Query;
