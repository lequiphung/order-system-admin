import {gql, useMutation, useQuery} from "@apollo/client";
import SHOP_LIST from "../../queries/shop/list";
import React from "react";
import SHOP_COUNT from "../../queries/shop/count";
import SHOP_FIND from "../../queries/shop/find";
//import SHOP_DELETE from "../../queries/shop/delete";

export {
    ListShop,
    CountShop,
    FindShop
}

function ListShop ({ children, query, id, pagination,setPagination,setLoading,setReload,where={} }){
    let limit = 10;
    let start = 0;

    if(pagination && pagination.pageSize){
        limit = pagination.pageSize;
    }
    if(pagination && pagination.current){
        start = (pagination.current - 1) * limit;
    }

    const { data, loading, error } = useQuery(SHOP_LIST, {
        variables: { id: id,limit:limit,start:start,where:where}
    });

    if (loading){
        //setLoading(true);
        return loading;

    }else{
        //setLoading(false);
    }
    if (error) return <p>Error: {JSON.stringify(error)}</p>;
    return children({ data });
}
function CountShop(where={}){
    const { data, loading, error } = useQuery(SHOP_COUNT, {
        variables: {where:where}
    });

    return data;
}

async function FindShop(id){
    let data = await getFindShop(id);
    return data;
}

function getFindShop(id){
    const { data, loading, error } = useQuery(SHOP_FIND, {
        variables: {id:id}
    });

    return data;
}