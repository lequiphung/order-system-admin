import React, {useEffect, useState} from "react";
import {Table, Space, Modal, Tag, message, notification} from "antd";
import $ from 'jquery';
import { ExclamationCircleOutlined } from '@ant-design/icons';
import {useMutation} from "@apollo/client";
import SHOP_DELETE from "../../queries/shop/delete";
import Link from "next/link";

const { confirm } = Modal;

const List = ({ partners,pagination,setPagination,loadingParent,setReload }) => {
    if(!partners){
        return <div></div>;
    }
    let rows = [];
    partners.partners.map((item,i) => {
        let imageUrl = '';
        if(!$.isEmptyObject(item.image)){
            imageUrl = process.env.NODE_ENV !== "development"
                ? item.image.url
                : process.env.API_BACKEND_URL + item.image.url;
        }
        rows.push(
            {
                id:item.id,
                key: item.id,
                index:i+1,
                username: item.user_id?.username,
                name: item.name,
                image: imageUrl,
                phone: item.phone,
                active: item.active
            }
        );
        return rows
    });

    const [loadingList,setLoadingList] = useState(false);
    const [deleteShop, { data,loading }] = useMutation(SHOP_DELETE);

    const successNotification = (message, placement="topRight") => {
        notification.success({
            message: message,
            placement,
        });
    };

    const showDeleteConfirm = (id) =>{
        confirm({
            title: 'Are you sure delete this shop?',
            icon: <ExclamationCircleOutlined />,
            //content: 'Some descriptions',
            okText: 'Yes',
            okType: 'danger',
            cancelText: 'No',
            onOk() {
                deleteShop({
                    variables: {
                        id:id
                    }
                });
            },
            onCancel() {
                console.log('Cancel');
                setReload(true);
            },
        });
    }

    useEffect(() => {
        if(!$.isEmptyObject(data) && !loading){
            successNotification('Shop deleted successfully');
            setReload(true);
        }
    },[data,loading]);

    useEffect(() => {
        if(loadingParent || loading){
            setLoadingList(true);
        }else{
            setLoadingList(false);
        }
    },[loadingParent,loading]);

    const columns = [
        {
            title: '#',
            dataIndex: 'index',
            width: 150,
        },
        {
            title: 'Username',
            dataIndex: 'username',
            width: 270,
        },
        {
            title: 'Name',
            dataIndex: 'name',
            width: 270,
        },
        {
            title: 'Image',
            dataIndex: 'image',
            width: 200,
            render: image => <img alt={image} src={image} height="50" />
        },
        {
            title: 'Phone',
            dataIndex: 'phone',
            width: 100,
        },
        {
            title: 'Status',
            dataIndex: 'active',
            width: 150,
            render: (text, record) => {
                return text?<Tag color="green">Active</Tag>:<Tag color="red">Inactive</Tag>;
            },
        },
        {
            title: 'Action',
            key: 'operation',
            fixed: 'right',
            width: 100,
            render: (text, record) => (
                <div className="action-btns">
                    <a onClick={() => showDeleteConfirm(record.id)}><i className="fa fa-trash"></i></a>
                    <Link href="/shop/[id]/edit" as={`/shop/${record.id}/edit`}>
                        <a className="edit-btn"><i className="fas fa-edit"></i></a>
                    </Link>
                </div>
            ),
        },
    ];

    return (
        <div>
            <Table
                columns={columns}
                //rowKey={record => record.login.uuid}
                dataSource={rows}
                pagination={pagination}
                loading={loadingList}
                onChange={setPagination}
            />

        </div>

    )

}

export default List