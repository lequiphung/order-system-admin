import {Button, Col, Form, Input, Layout, Row, Spin, Switch,TimePicker} from 'antd';
import Link from "next/link";
import UploadImage from "./upload-image";
import UploadQrcode from "./upload-qrcode";
import Wrapper from "../Wrapper";
import React from "react";
import moment from "moment";
import { ArrowLeftOutlined } from '@ant-design/icons';
const { Content } = Layout;
const format = 'HH:mm';
const UpdateForm = ({ object,onFinish,loading,setImage,setPaypayQrcode, action }) => {
    const [form] = Form.useForm();

    let initialValues = {};
    if(object){
        initialValues = {
            name:object.name,
            phone:object.phone,
            address:object.address,
            open_hours:object.open_hours && moment(object.open_hours, format),
            close_hours:object.close_hours && moment(object.close_hours, format),
            longitude:object.longitude,
            latitude:object.latitude,
            active:object.active,
            censorship:object.censorship,
            user: {
                username:object.user_id.username,
                email:object.user_id.email,
                confirmed:object.user_id.confirmed,
            },
        }
    }

    const onClear = () =>{
        form.resetFields();
    }
    return (
        <Content>

            <Spin spinning={loading}>

                <main>
                    <div className="container-fluid">
                        <h2 className="mt-30 page-title">Shop</h2>
                        <ol className="breadcrumb mb-30">
                            <li className="breadcrumb-item"><Link href="/shop"><a>Shop</a></Link></li>
                            <li className="breadcrumb-item active">{action}</li>

                        </ol>
                        <div className="row">
                            <div className="col-lg-12 col-md-12 mt-30">
                                <Col>
                                    <Link href="/shop">
                                        <Button type="danger" className="custom-mg-icon" icon={<ArrowLeftOutlined />} >
                                            Back
                                        </Button>
                                    </Link>
                                </Col>
                            </div>
                            <div className="col-lg-12">
                                <div className="add-new-shop">
                                    <div className="card card-static-2 mb-30">
                                        <Form
                                            form={form}
                                            name="advanced_search"
                                            className="ant-advanced-search-form"
                                            onFinish={onFinish}
                                            initialValues={initialValues}
                                        >
                                            <div className="row no-gutters">

                                                <div className="col-lg-12 col-md-12">
                                                    <div className="card-body-table"  style={{ "paddingTop": '20px',}}>
                                                        <div className="add-shop-content pd-20 row">

                                                            <div className="col-lg-6 col-md-6">
                                                                <div className="form-group">
                                                                    <label className="form-label">Username <span className={'required'}>*</span></label>
                                                                    <Form.Item name={['user', 'username']} rules={[
                                                                        {
                                                                            required: true,
                                                                            message: 'Please input username!',
                                                                        },
                                                                    ]}>
                                                                        <Input type="text" className="form-control" placeholder="Username"/>
                                                                    </Form.Item>
                                                                </div>
                                                            </div>
                                                            <div className="col-lg-6 col-md-6">
                                                                <div className="form-group">
                                                                    <label className="form-label">Password {!object && <span className={'required'}>*</span>}</label>
                                                                    <Form.Item name={['user', 'password']} rules={!object && [
                                                                        {
                                                                            required: true,
                                                                            message: 'Please input Password!',
                                                                        },
                                                                    ]}>
                                                                        <Input type="password" className="form-control" placeholder="Password" />
                                                                    </Form.Item>
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <div className="add-shop-content pd-20 row">

                                                            <div className="col-lg-6 col-md-6">

                                                                <div className="form-group">
                                                                    <label className="form-label">Name <span className={'required'}>*</span></label>
                                                                    <Form.Item name="name" rules={[
                                                                        {
                                                                            required: true,
                                                                            message: 'Please input Name!',
                                                                        },
                                                                    ]}>
                                                                        <Input type="text" className="form-control" placeholder="Name"/>
                                                                    </Form.Item>
                                                                </div>
                                                                <div className="form-group">
                                                                    <label className="form-label">Email <span className={'required'}>*</span></label>
                                                                    <Form.Item name={['user', 'email']} rules={[
                                                                        {
                                                                            required: true,
                                                                            message: 'Please input email!',
                                                                        },
                                                                        {
                                                                            type: 'email',
                                                                            message: 'Email is not validate email!',
                                                                        },
                                                                    ]}>
                                                                        <Input type="text" className="form-control" placeholder="Email"/>
                                                                    </Form.Item>
                                                                </div>

                                                                <div className="form-group">
                                                                    <label className="form-label">Phone number <span className={'required'}>*</span></label>
                                                                    <Form.Item name="phone" rules={[
                                                                        {
                                                                            required: true,
                                                                            message: 'Please input phone number',
                                                                        },
                                                                    ]}>
                                                                        <Input type="text" className="form-control" placeholder="Phone number"/>
                                                                    </Form.Item>
                                                                </div>

                                                                <div className="form-group">
                                                                    <label className="form-label">Address</label>
                                                                    <Form.Item name="address">
                                                                        <Input type="text" className="form-control" placeholder="Address"/>
                                                                    </Form.Item>
                                                                </div>
                                                            </div>
                                                            <div className="col-lg-6 col-md-6">
                                                                <div className="form-group">
                                                                    <label className="form-label">Open hours</label>
                                                                    <Form.Item name="open_hours">
                                                                        <TimePicker
                                                                            //defaultValue={moment(object.open_hours, format)}
                                                                            format={format}
                                                                        />
                                                                    </Form.Item>
                                                                </div>

                                                                <div className="form-group">
                                                                    <label className="form-label">Close hours</label>
                                                                    <Form.Item name="close_hours">
                                                                        <TimePicker
                                                                            //defaultValue={object.close_hours && moment(object.close_hours, format)}
                                                                            format={format}
                                                                        />
                                                                    </Form.Item>
                                                                </div>

                                                                <div className="form-group">
                                                                    <label className="form-label">Longitude</label>
                                                                    <Form.Item name="longitude">
                                                                        <Input type="number" className="form-control" placeholder="Longitude"/>
                                                                    </Form.Item>
                                                                </div>
                                                                <div className="form-group">
                                                                    <label className="form-label">Latitude</label>
                                                                    <Form.Item name="latitude">
                                                                        <Input type="number" className="form-control" placeholder="Latitude"/>
                                                                    </Form.Item>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div className="add-shop-content pd-20">
                                                            <div className="col-lg-12 col-md-12">
                                                                <div className="form-group">
                                                                    <label className="form-label">Image</label>
                                                                    <UploadImage setImage={setImage} image={object && object.image?object.image:false}/>
                                                                </div>
                                                                <div className="form-group">
                                                                    <label className="form-label">Paypay qrcode</label>
                                                                    <UploadQrcode setPaypayQrcode={setPaypayQrcode} image={object && object.paypay_qrcode?object.paypay_qrcode:false}/>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div className="add-shop-content pd-20">
                                                            <div className="col-lg-12 col-md-12">
                                                                <Row gutter={3}>
                                                                    <Form.Item name={['user', 'confirmed']} label="Confirmed" valuePropName="checked">
                                                                        <Switch  defaultChecked={object && object.user_id.confirmed?true:false}/>
                                                                    </Form.Item>
                                                                    <Form.Item name="active" label="Status" valuePropName="checked" className="ml-30">
                                                                        <Switch defaultChecked={object && object.active?true:false}/>
                                                                    </Form.Item>
                                                                </Row>
                                                            </div>
                                                        </div>

                                                        <div className="add-shop-content pd-20">
                                                            <div className="col-lg-12 col-md-12">
                                                                <Col span={24} style={{ textAlign: 'right',}}>
                                                                    <Button type="danger" htmlType="submit" className="status-btn hover-btn"> Submit</Button>
                                                                    <Button style={{ margin: '0 8px'}} onClick={onClear}>Reset</Button>
                                                                </Col>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </Form>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>

            </Spin>
        </Content>
    )
}
export default UpdateForm
