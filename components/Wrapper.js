import React, {useEffect, useState} from 'react'
import Router, {useRouter} from 'next/router'
import {useDispatch, useSelector} from 'react-redux'
//import Footer from "./includes/Footer";
import Nav from "./includes/Nav";
import Head from "next/head";

import {Layout, Menu, Breadcrumb, Spin, Skeleton} from 'antd';
import Link from "next/link";
import axios from "axios";
import * as types from "../constants/types";

const { Header, Content, Footer, Sider } = Layout;
const { SubMenu } = Menu;

export default function Wrapper(props){
    const { children, title } = props;
    const { token } = useSelector((state) => state.loginReducer);
    const [loading,setLoading] = useState(false);
    const router = useRouter();
    const dispatch = useDispatch();

    useEffect(() => {
        if(!token){
            Router.push('/login')
        }else{
            axios.get(process.env.API_BACKEND_URL+'/users-permissions/user-info',{
                headers: {
                    Authorization: 'Bearer '+token,
                }
            }).then(res => {
            }).catch(res => {
                Router.push('/logout');
            });
        }
    });

    useEffect(() => {
        dispatch({
            type: types.ROUTE_HISTORY,
            pathname : router.pathname
        });
    },[]);



    if(!token) return null;

    return (
        <div>
            <Spin spinning={loading}>

            {/*<Layout style={{ minHeight: '100vh' }}>*/}
            {/*    <Sider collapsible collapsed={collapsed} onCollapse={setCollapsed}>*/}
            {/*        <div className="logo" />*/}
            {/*        <Menu theme="dark" defaultSelectedKeys={['shop_list']} defaultOpenKeys={['shop']} mode="inline">*/}
            {/*            /!*<Menu.Item key="1" icon={<PieChartOutlined />}>*!/*/}
            {/*            /!*    Option 1*!/*/}
            {/*            /!*</Menu.Item>*!/*/}
            {/*            <SubMenu key="shop" icon={<ShopOutlined />} title="Shop">*/}
            {/*                <Menu.Item key="shop_list"><Link href={`/shop`}><a>List</a></Link></Menu.Item>*/}
            {/*                <Menu.Item key="shop_create">Creat</Menu.Item>*/}
            {/*            </SubMenu>*/}

            {/*            /!*<Menu.Item key="9" icon={<FileOutlined />} />*!/*/}
            {/*        </Menu>*/}
            {/*    </Sider>*/}
            {/*    <Layout className="site-layout">*/}
            {/*        <Header className="site-layout-background" style={{ padding: 0 }} />*/}

            {/*        {children}*/}

            {/*        <Footer style={{ textAlign: 'center' }}>Ant Design ©2018 Created by Ant UED</Footer>*/}
            {/*    </Layout>*/}
            {/*</Layout>*/}

                <Head>
                    {title && <title>{title}</title>}
                    <meta name="viewport" content="width=device-width, initial-scale=1"/>
                    <link href="/html/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
                    <link href="/html/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" />
                    <link href="/html/css/admin-style.css" rel="stylesheet" />
                    <link href="/html/css/styles.css" rel="stylesheet" />
                    <script src={"/html/js/jquery-3.4.1.min.js"}></script>
                    <script src={"/html/vendor/bootstrap/js/bootstrap.bundle.min.js"}></script>
                    <script src={"/html/js/scripts.js"}></script>
                </Head>
                <Layout>
                    <div className="sb-nav-fixed">
                        <nav className="sb-topnav navbar navbar-expand navbar-light bg-clr">
                            <a className="navbar-brand logo-brand" href="/">Order System</a>
                            <button className="btn btn-link btn-sm order-1 order-lg-0" id="sidebarToggle" href="#"><i className="fas fa-bars"></i></button>
                            <ul className="navbar-nav ml-auto mr-md-0">
                                <li className="nav-item dropdown">
                                    <a className="nav-link " id="userDropdown" href="#" role="button"><i className="fas fa-user fa-fw"></i></a>
                                    <div className="dropdown-menu dropdown-menu-right userDropdownItem" >
                                        <a className="dropdown-item admin-dropdown-item" href="edit_profile.html">Edit Profile</a>
                                        <a className="dropdown-item admin-dropdown-item" href="change_password.html">Change Password</a>
                                        <Link href="/logout"><a className="dropdown-item admin-dropdown-item">Logout</a></Link>
                                    </div>
                                </li>
                            </ul>
                        </nav>
                        <div id="layoutSidenav">
                            <Nav />

                            <div id="layoutSidenav_content">

                                {children}

                                <footer className="py-4 bg-footer mt-auto">
                                    <div className="container-fluid">
                                        <div className="d-flex align-items-center justify-content-between small">
                                            <div className="text-muted-1">© 2020 <b>Gambo Supermarket</b>. by <a
                                                href="https://themeforest.net/user/gambolthemes">Gambolthemes</a></div>
                                        </div>
                                    </div>
                                </footer>
                            </div>
                        </div>

                    </div>
                </Layout>
            </Spin>
        </div>
    )
}