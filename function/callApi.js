import axios from "axios";

export {
    get,
    checkToken
}

function getToken() {
    //if (typeof window === 'undefined') return '';

    const token = localStorage.getItem('token');
    return token ? `Bearer ${token}` : '';
};

async function get(action){
    const url = process.env.API_BACKEND_URL+action;
    return await axios.get(url,{
        headers: {
            Authorization: getToken(),
        }
    }).then(res => {
        console.log(res.data);
        return res.data;
    }).catch(res => {
        return false;
    });
}

function checkToken(){
    //const a = get('/users-permissions/user-info');
    return axios.get(process.env.API_BACKEND_URL+'/users-permissions/user-info',{
        headers: {
            Authorization: getToken(),
        }
    }).then(res => {
        console.log(res.data);
        return res.data;
    }).catch(res => {
        return false;
    });
}