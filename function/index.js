import $ from "jquery";
import {notification} from "antd";

export {
    formatDataForm,
    failNotification,
    successNotification
}

function formatDataForm(values,unset_empty=false){
    let data = {};
    Object.entries(values).forEach(entry => {
        let [key, value] = entry;
        if($.type(value) == 'object'){
            let tmp = formatDataForm(value,unset_empty);
            if(!$.isEmptyObject(tmp)){
                data[key] = tmp;
            }
        }else{
            if(value!= undefined){
                if(unset_empty){
                    if(value != ''){
                        data[key] = value;
                    }
                }else{
                    data[key] = value;
                }
            }
        }
    });

    return data;
}

const successNotification = (message, placement="topRight") => {
    notification.success({
        message: message,
        placement,
    });
};

const failNotification = (message, placement="topRight") => {
    notification.warn({
        message: message,
        placement,
    });
};