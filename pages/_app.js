import { ApolloProvider } from '@apollo/client'
import { Provider } from 'react-redux'
import { persistStore } from 'redux-persist'
import { PersistGate } from 'redux-persist/integration/react'
import { useStore } from '../store'
import { useApollo } from '../services/apollo'
import 'antd/dist/antd.css';

export default function App({ Component, pageProps }) {
  const store = useStore(pageProps.initialReduxState)
  const apolloClient = useApollo(pageProps.initialApolloState)
  const persistor = persistStore(store)

  return (
    <Provider store={store}>
      <ApolloProvider client={apolloClient}>
        <PersistGate loading={<Component {...pageProps} />} persistor={persistor}>         
          <Component {...pageProps} />
        </PersistGate>
      </ApolloProvider>
    </Provider>
  )
}
