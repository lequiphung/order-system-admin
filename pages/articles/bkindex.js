import Head from 'next/head'
import Wrapper from '../../components/Wrapper'
import React from "react";
import Query from "../../components/Query";
import ARTICLES_QUERY from "../../queries/article/articles";
import List from "../../components/Articles/List";
import CallApi from "../../services";


export default function Articles() {
    // const url = "http://localhost:1337/articles/count";
    return (
        <Wrapper title="Articles">
            <div className="content-wrapper">
                <section className="content-header">
                    <div className="container-fluid">
                        <div className="row mb-2">
                            <div className="col-sm-6">
                                <h1>Articles</h1>
                            </div>
                            <div className="col-sm-6">
                                <ol className="breadcrumb float-sm-right">
                                    <li className="breadcrumb-item active">Articles</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </section>

                <section className="content">

                    <div className="card">
                        <div className="card-header">
                            <h3 className="card-title">Articles</h3>

                            <div className="card-tools">
                                <button type="button" className="btn btn-tool" data-card-widget="collapse"
                                        data-toggle="tooltip" title="Collapse">
                                    <i className="fas fa-minus"></i></button>
                                <button type="button" className="btn btn-tool" data-card-widget="remove"
                                        data-toggle="tooltip" title="Remove">
                                    <i className="fas fa-times"></i></button>
                            </div>
                        </div>
                        <div className="card-body p-0">

                            <CallApi url="http://localhost:1337/articles/count" method="get">
                                {({ data: { count } }) => {
                                    return(<p>Phung {count}</p>)

                                }}

                            </CallApi>

                            <Query query={ARTICLES_QUERY}  >
                                {({ data: { articles } }) => {
                                    return (
                                        <List articles={articles} />
                                    );
                                }}
                            </Query>
                        </div>

                    </div>

                </section>
            </div>
        </Wrapper>
    )
}
