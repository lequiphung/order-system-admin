import React from 'react';
import axios from 'axios';
import Query from "../../components/Query";
import ARTICLES_QUERY from "../../queries/article/articles";
import List from "../../components/Articles/List";
import Wrapper from "../../components/Wrapper";
import {Table} from "antd";
import {useQuery} from "@apollo/client";


const columns = [
    {
        title: '#',
        dataIndex: 'index',
        width: 150,
    },
    {
        title: 'Title',
        dataIndex: 'title',
        width: 270,
    },
    {
        title: 'Image',
        dataIndex: 'image',
        width: 200,
        render: image => <img alt={image} src={image} height="50" />
    },
    {
        title: 'Category',
        dataIndex: 'category',
        width: 100,
    },
    {
        title: 'Status',
        dataIndex: 'status',
        sort: 'disabled',
        width: 150,
    },
    {
        title: 'Action',
        key: 'operation',
        fixed: 'right',
        width: 100,
        render: () => <a>action</a>,
    },
];

const getParams = params => {
    let limit = params.pagination.pageSize;
    let    page = params.pagination.current;
    let start = (page - 1) * limit;

    return {
        limit: limit,
        start: start,
        ...params,
    };
};

export default class Articles extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            count: 0,
            data: [],
            limit:1,
            start:0,
            pagination: {
                current: 1,
                pageSize: 1,
            },
            loading: false,
        }
    }
    static async getInitialProps(ctx) {
        return ctx.query;
    }
    componentDidMount() {
        axios.get(`http://localhost:1337/articles/count`)
            .then(res => {
                this.setState({ count:res.data });
            })
        console.log(this.state.count);
        const { pagination } = this.state;
        this.fetch({ pagination });

    }

    handleTableChange = (pagination, filters, sorter) => {
        this.fetch({
            sortField: sorter.field,
            sortOrder: sorter.order,
            pagination,
            ...filters,
        });
    };

    fetch = (params = {}) => {
        let data_params = getParams(params);
        this.setState({ loading: true,limit:data_params.limit,start:data_params.start });





        axios.get(`http://localhost:1337/articles?_limit=`+data_params.limit+`&_start=`+data_params.start)
            .then(res => {
                console.log(this.state.count);
                let rows = []
                res.data.forEach((article,i) =>{
                    const imageUrl = process.env.NODE_ENV !== "development"
                        ? article.image.url
                        : process.env.REACT_APP_BACKEND_URL + article.image.url;
                    rows.push(
                        {
                            key: i,
                            index:i+1,
                            title: article.title,
                            image:  imageUrl,
                            category: article.category.name,
                            status: "Success",
                            function: ''
                        }
                    );
                });
                this.setState({
                    loading: false,
                    data: rows,
                    pagination: {
                        ...params.pagination,
                        total: this.state.count,
                        // 200 is mock data, you should read it from server
                        // total: data.totalCount,
                    },
                });
                console.log(res.data);
            })
    };

    render() {
        const { data, pagination, loading, limit, start } = this.state;
        const params = {
            limit : limit,
            start : start
        }
        return (
            <Wrapper title="Articles">
                <div className="content-wrapper">
                    <section className="content-header">
                        <div className="container-fluid">
                            <div className="row mb-2">
                                <div className="col-sm-6">
                                    <h1>Articles</h1>
                                </div>
                                <div className="col-sm-6">
                                    <ol className="breadcrumb float-sm-right">
                                        <li className="breadcrumb-item active">Articles</li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </section>

                    <section className="content">

                        <div className="card">
                            <div className="card-header">
                                <h3 className="card-title">Articles</h3>

                                <div className="card-tools">
                                    <button type="button" className="btn btn-tool" data-card-widget="collapse"
                                            data-toggle="tooltip" title="Collapse">
                                        <i className="fas fa-minus"></i></button>
                                    <button type="button" className="btn btn-tool" data-card-widget="remove"
                                            data-toggle="tooltip" title="Remove">
                                        <i className="fas fa-times"></i></button>
                                </div>
                            </div>
                            <div className="card-body p-0">
                                <Table
                                    columns={columns}
                                    //rowKey={record => record.login.uuid}
                                    dataSource={data}
                                    pagination={pagination}
                                    loading={loading}
                                    onChange={this.handleTableChange}
                                />

                                <Query query={ARTICLES_QUERY} params={params}>
                                    {({ data: { articles } }) => {
                                        return (
                                            <List articles={articles} count={this.state.count} />
                                        );
                                    }}
                                </Query>
                            </div>
                        </div>

                    </section>
                </div>
            </Wrapper>
        )
    }
}