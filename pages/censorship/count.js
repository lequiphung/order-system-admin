import React, {useEffect} from "react";
import {Button} from "antd";
import {useDispatch, useSelector} from "react-redux";
import ListCensorshipShop from "../../components/Censorship/shop";
import {useState} from "react";
import * as types from "../../constants/types";
import $ from "jquery";
import {successNotification} from "../../function";


// const sagaMiddleware = createSagaMiddleware()
// const store = createStore(
//     Censorship,
//     applyMiddleware(sagaMiddleware)
// )
//
// sagaMiddleware.run(rootSaga)

const CensorshipShop = () =>{
    const dispatch = useDispatch();
    const {count} = useSelector(state => state.CensorShip);

    console.log(count);
    const onIncrement = () => {
        dispatch({ type: 'INCREMENT' });
    }

    useEffect(() => {
        dispatch({type:"CENSORSHIP"})
    },[]);

    useEffect(() => {
        console.log('count change');
    },[count]);

    // const login = async data => {
    //     if(!data){
    //         data = await dispatch({
    //             type: types.LOGIN_REQUEST,
    //             username:"phung",
    //             password:"1234",
    //         });
    //     }
    // }


    return (
        <div>
            This is clicked {count} times!
            <Button onClick={() => dispatch({type:"CENSORSHIP"})}>click me</Button>
            <button onClick={onIncrement}>+</button>
            <button onClick={() => dispatch({
                type: types.LOGIN_REQUEST,
                username:"phung",
                password:"1234",
            })}>Login</button>

            <ListCensorshipShop/>
        </div>
    )
}

export default CensorshipShop;

//store.subscribe(CensorshipShop)