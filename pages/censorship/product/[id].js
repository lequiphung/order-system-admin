import React from "react";
import {useRouter} from "next/router";
import QueryFind from "../../../components/Query/find";
import Show from "../../../components/Censorship/product/show";
import Wrapper from "../../../components/Wrapper";
import {Layout} from "antd";
import {FIND_WITH_PARENT} from "../../../queries/product";
const { Content } = Layout;
const Id = () =>{

    const router = useRouter()
    const { id } = router.query;

    return (
        <Wrapper>
            <Content>
                <QueryFind query={FIND_WITH_PARENT} id={id}>
                    {({ data: { product } }) => {
                        return (
                            <Show product={product}/>
                        );
                    }}
                </QueryFind>
            </Content>
        </Wrapper>
    )
}
export default Id