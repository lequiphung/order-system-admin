import React from "react";
import {useRouter} from "next/router";
import {FIND_WITH_PARTNER_PARENT} from "../../../queries/user";
import QueryFind from "../../../components/Query/find";
import Show from "../../../components/Censorship/shop/show";
import Wrapper from "../../../components/Wrapper";
import {Layout} from "antd";
const { Content } = Layout;
const Id = () =>{

    const router = useRouter()
    const { id } = router.query;

    return (
        <Wrapper>
            <Content>
                <QueryFind query={FIND_WITH_PARTNER_PARENT} id={id}>
                    {({ data: { user } }) => {
                        return (
                            <Show partner={user.partner_id} user={user} />
                        );
                    }}
                </QueryFind>
            </Content>
        </Wrapper>
    )
}
export default Id