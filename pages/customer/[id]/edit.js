import PartnerEdit from "../../../components/Partner/Edit";
import {useDispatch} from "react-redux";
import React, {useEffect} from "react";
import * as types from "../../../constants/types";
import Wrapper from "../../../components/Wrapper";
const CustomerEdit = () =>{
    const object_info = {
        title:'Customer',
        action:'Create Customer',
        link_back: '/customer',
        customer:true,
        shop:false
    }
    const dispatch = useDispatch();
    useEffect(() =>{
        dispatch({
            type:types.PARTNER_SET_OBJECT_INFO,
            object_info:object_info
        });
    },[]);

    return (
        <Wrapper>
            <PartnerEdit/>
        </Wrapper>
    )
}

export default CustomerEdit