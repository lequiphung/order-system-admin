import {LIST} from "../../queries/partner";
import React, {useEffect, useState} from "react";
import $ from "jquery";
import {useLazyQuery} from "@apollo/client";
import PartnerList from "../../components/Partner/List";
import Wrapper from "../../components/Wrapper";

const Customer = () => {
    const object_info ={
        title:'Customer',
        create_link:'/customer/create',
        edit_prefix:'/customer/',
    }
    const where_default = {
        partner_id:{
            customer:true
        }
    }
    const pagination_default = {
        current: 1,
        pageSize: process.env.pageSize??10,
        total:0
    }

    const [where,setWhere] = useState(where_default);
    const [pagination,setPagination] = useState(pagination_default);
    const [reload,setReload] = useState(false);

    const [getPartners, { called, loading, data}] = useLazyQuery(LIST,
        { variables: { limit:pagination.pageSize,start:(pagination.current - 1) * pagination.pageSize,where: where },fetchPolicy: 'network-only' }
    );
    useEffect(() => {
        if(!called){
            getPartners();
        }
    },[called]);

    useEffect(() => {
        if(reload){
            setPagination(pagination_default);
            getPartners({ variables: { limit:pagination_default.pageSize,start:(pagination_default.current - 1) * pagination_default.pageSize,where: where } });
        }
        setReload(false);
    },[reload]);

    useEffect(() => {
        setPagination({...pagination, current : 1});
    },[where]);
    useEffect(() => {
        if(!$.isEmptyObject(data) && !loading){
            setPagination({...pagination,total:data.usersConnection.aggregate.count});
        }
    },[data,loading]);


    return (
        <Wrapper>
            <PartnerList
                partners={data}
                setPagination={setPagination}
                pagination={pagination}
                loadingParent={loading}
                setReload={setReload}
                setWhere={setWhere}
                where_default={where_default}
                object_info={object_info}
            />
        </Wrapper>
    )
};

export default Customer;