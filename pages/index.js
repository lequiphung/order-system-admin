import Wrapper from '../components/Wrapper'
import React from "react";
import {Layout} from "antd";
const { Content } = Layout;
function Home() {
  return (
      <Wrapper title="Articles">
          <Layout>
              <Content>
                  <main>
                      <div className="container-fluid">
                          <div className="row justify-content-between">
                              Welcome to Order System
                          </div>
                      </div>
                  </main>
              </Content>
          </Layout>
      </Wrapper>
  )
}

export default Home
