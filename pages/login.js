import React, { useState, useEffect } from 'react';
import {Button, Form, Input, Layout} from "antd";
import Head from "next/head";
import {loginUser} from "../services/userServices";
import {useMutation} from "@apollo/client";
import {LOGIN} from "../queries/user";
import {failNotification} from "../function";
import {useDispatch, useSelector} from "react-redux";
import * as loginActions from "../actions/loginActions";
import Router from 'next/router'
import $ from 'jquery';

function Login() {
    const [form] = Form.useForm();
    const [login,{data,loading,error}] = useMutation(LOGIN);
    const dispatch = useDispatch();
    const { token } = useSelector((state) => state.loginReducer);
    const { history } = useSelector((state) => state.routeHistoryReducer);


    useEffect(() => {
        if(token){
            let pathname = '/';
            if(history.length){
                pathname = history[history.length - 1];
            }
            Router.push(pathname)
        }
    }, [token]);

    const onSubmit = values => {
        login({ variables: {
            input: {
                identifier: values.email,
                password: values.password,
                provider: 'local'
            }
        }}).catch((e) =>{
            console.log(e);
        });

    }
    useEffect(() =>{
        if(!$.isEmptyObject(data) && !loading){
            console.log(loginActions.loginSuccess( data.login ));
            localStorage.setItem('token',data.login.jwt);
            dispatch(loginActions.loginSuccess( data.login ));
            Router.push('/')
        }
        if(error){
            failNotification('Email or password is not correct!');
        }

    },[data,loading,error])

    return (
        <div>
            <Layout>
                <Head>
                    <title>Login</title>}
                    <meta name="viewport" content="width=device-width, initial-scale=1"/>
                    <link href="/html/css/styles.css" rel="stylesheet" />
                    <link href="/html/css/admin-style.css" rel="stylesheet" />
                    <link href="/html/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
                    <link href="/html/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" />
                </Head>

                <div id="layoutAuthentication">
                    <div id="layoutAuthentication_content">
                        <main>
                            <div className="container">
                                <div className="row justify-content-center">
                                    <div className="col-lg-5">
                                        <div className="card shadow-lg border-0 rounded-lg mt-5">
                                            <div className="card-header card-sign-header">
                                                <h3 className="text-center font-weight-light my-4">Login</h3>
                                            </div>
                                            <div className="card-body">
                                                <Form
                                                    form={form}
                                                    name="form_login"
                                                    onFinish={onSubmit}
                                                >
                                                    <div className="form-group">
                                                        <label className="form-label"
                                                               htmlFor="inputEmailAddress">Email*</label>

                                                        <Form.Item name={'email'} rules={[
                                                            {
                                                                required: true,
                                                                message: 'Please input Email!',
                                                            },
                                                            {
                                                                type: 'email',
                                                                message: 'Email is not validate email!',
                                                            }
                                                        ]}>
                                                            <Input type="text" className="form-control" placeholder="Email"/>
                                                        </Form.Item>
                                                    </div>
                                                    <div className="form-group">
                                                        <label className="form-label"
                                                               htmlFor="inputPassword">Password*</label>
                                                        <Form.Item name="password" rules={[
                                                            {
                                                                required: true,
                                                                message: 'Please input Password!',
                                                            }
                                                        ]}>
                                                            <Input type="password" className="form-control" placeholder="Password" />
                                                        </Form.Item>
                                                    </div>
                                                    <div className="form-group">
                                                        <div className="custom-control custom-checkbox">
                                                            <input className="custom-control-input"
                                                                   id="rememberPasswordCheck" type="checkbox"/>
                                                            <label className="custom-control-label"
                                                                   htmlFor="rememberPasswordCheck">Remember
                                                                password</label>
                                                        </div>
                                                    </div>
                                                    <div className="form-group d-flex align-items-center justify-content-between mt-4 mb-0">
                                                        <Button htmlType="submit" className="btn btn-sign hover-btn">Login</Button>
                                                    </div>
                                                </Form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </main>
                    </div>
                </div>
            </Layout>

            <script src="/html/js/jquery-3.4.1.min.js"></script>
            <script src="/html/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        </div>
    )
}


export default Login;