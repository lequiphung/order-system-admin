import {useEffect} from "react";
import * as loginActions from "../actions/loginActions";
import Router from "next/router";
import {useDispatch} from "react-redux";


const Logout = () => {
    const dispatch = useDispatch();
    useEffect(() =>{
        dispatch(loginActions.loginOut());
        localStorage.removeItem('token');
        Router.push('/login');
    },[]);

    return <div></div>;
}
export default Logout;