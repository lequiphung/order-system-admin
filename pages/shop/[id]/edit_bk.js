import React, {useEffect, useState} from "react";
import Wrapper from "../../../components/Wrapper";
import {Layout, Modal} from 'antd';
import $ from "jquery";
import {formatDataForm,failNotification} from "../../../function";
import moment from 'moment';
import {useLazyQuery, useMutation, useQuery} from "@apollo/client";
import USER_LIST from "../../../queries/user/find";
import Router, {useRouter} from "next/router";
import SHOP_FIND from "../../../queries/shop/find";
import SHOP_UPDATE from "../../../queries/shop/update";
import USER_UPDATE from "../../../queries/user/update";
import UpdateForm from "../../../components/Shop/update-form";

const { Content } = Layout;
const format = 'HH:mm';

const ShopEdit = () => {
    const router = useRouter()
    const { id } = router.query
    const { loading:loading_find, error, data:partner } = useQuery(SHOP_FIND, {
        variables: {id:id}
    });
    const [object,setObject] = useState(false);
    const [data,setData]    = useState({});
    const [image,setImage]  = useState('');
    const [paypay_qrcode,setPaypayQrcode] = useState('');
    const [checking,setChecking] = useState(false);
    const [submit,setSubmit] = useState(false);
    const [loading,setLoading] = useState(false);

    const [getUsers, { called:called_user, loading:loading_users, data:users}] = useLazyQuery(USER_LIST);

    const [updateShop, { data:update_shop,loading:loading_shop }] = useMutation(SHOP_UPDATE);

    const [updateUser, { data:update_user,loading:loading_user }] = useMutation(USER_UPDATE);

    useEffect(() => {
        if(partner !=  undefined && !loading_find){
            setObject(partner.partner);
        }
    },[partner,loading_find]);

    useEffect(() => {
        if(loading_users || loading_shop || loading_user){
            setLoading(true);
        }else{
            setLoading(false);
        }
    },[loading_users,loading_shop,loading_user]);

    useEffect(() => {
        if(users !=  undefined && !loading_users){
            console.log(users.users);
            if(users.users.length){
                failNotification('User already exists!')
                setSubmit(false);
            }else{
                setSubmit(true);
            }
            setChecking(false);
        }

    },[loading_users,users]);

    useEffect(() => {
        if(!$.isEmptyObject(update_shop) && !loading_shop){
            Modal.success({
                content: 'Update Shop successfully',
                onOk() {
                    Router.push('/shop');
                },
            });
        }
    },[update_shop,loading_shop]);

    useEffect(() => {
        if(!$.isEmptyObject(update_user) && !loading_user){
            const data_shop = data;
            data_shop["user_id"] = object.user_id.id;
            delete data_shop["user"];
            updateShop({
                variables:{
                    input:{
                        where: { id: id },
                        data:data_shop
                    }
                }
            });
        }
    },[update_user,loading_user]);

    useEffect(() => {
        if(!checking && submit){
            updateUser({
                variables:{
                    input:{
                        where: {
                            id: object.user_id.id
                        },
                        data:data.user
                    }
                }
            });
        }

    },[submit]);

    const checkUser = (data) => {
        setChecking(true);
        getUsers({
            variables:{
                where:{
                    _or:[{username:data.username},{email:data.email}],
                    id_ne:object.user_id.id
                }
            }
        });
    }

    const onFinish = values => {
        const data_form = formatDataForm(values);
        if(data_form.open_hours){
            data_form.open_hours = moment(data_form.open_hours).format(format);
        }else{
            data_form.open_hours = '';
        }
        if(data_form.close_hours){
            data_form.close_hours = moment(data_form.close_hours).format(format);
        }else{
            data_form.close_hours = '';
        }
        if(data_form.longitude){
            data_form.longitude = parseFloat(data_form.longitude);
        }else{
            data_form.longitude = 0;
        }
        if(data_form.latitude){
            data_form.latitude = parseFloat(data_form.latitude);
        }else{
            data_form.latitude = 0;
        }

        setData(data => ({...data_form,image:image,paypay_qrcode:paypay_qrcode}));
        checkUser(data_form.user);
    };

    if(!object){
        return (
            <Wrapper>
                <Content>
                    Data not found!
                </Content>
            </Wrapper>
        )
    }

    return <UpdateForm object={object} onFinish={onFinish} loading={loading} setImage={setImage} setPaypayQrcode={setPaypayQrcode} action="Update Id"/>
}

export default ShopEdit