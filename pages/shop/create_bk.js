import React, {useEffect, useState} from "react";
import { Modal } from 'antd';
import $ from "jquery";
import {formatDataForm,failNotification} from "../../function";
import moment from 'moment';
import {useLazyQuery, useMutation, useQuery} from "@apollo/client";
import SHOP_CREATE from "../../queries/shop/create";
import USER_CREATE from "../../queries/user/create";
import USER_LIST from "../../queries/user/find";
import Router from "next/router";
import UpdateForm from "../../components/Shop/update-form";

const format = 'HH:mm';

const ShopCreate = () => {
    const [data,setData] = useState({});
    const [image,setImage] = useState('');
    const [paypay_qrcode,setPaypayQrcode] = useState('');
    const [checking,setChecking] = useState(false);
    const [submit,setSubmit] = useState(false);
    const [loading,setLoading] = useState(false);

    const [getUsers, { called:called_user, loading:loading_users, data:users}] = useLazyQuery(USER_LIST);
    const [createShop, { data:create_shop,loading:loading_shop }] = useMutation(SHOP_CREATE);
    const [createUser, { data:create_user,loading:loading_user }] = useMutation(USER_CREATE);

    useEffect(() => {
        if(loading_users || loading_shop || loading_user){
            setLoading(true);
        }else{
            setLoading(false);
        }
    },[loading_users,loading_shop,loading_user]);

    useEffect(() => {
        if(users !=  undefined && !loading_users){
            console.log(users.users);
            if(users.users.length){
                failNotification('User already exists!')
                setSubmit(false);
            }else{
                setSubmit(true);
            }
            setChecking(false);
        }

    },[loading_users,users]);

    useEffect(() => {
        if(!$.isEmptyObject(create_shop) && !loading_shop){
            Modal.success({
                content: 'Create Shop successfully',
                onOk() {
                    Router.push('/shop');
                },
            });
        }
    },[create_shop,loading_shop]);

    useEffect(() => {
        if(!$.isEmptyObject(create_user) && !loading_user){
            console.log(data);
            const data_shop = data;
            data_shop["user_id"] = create_user.createUser.user.id;
            data_shop["supplier"] = true;
            delete data_shop["user"];
            createShop({
                variables:{
                    input:{
                        data:data_shop
                    }
                }
            });
        }
    },[create_user,loading_user]);

    useEffect(() => {
        if(!checking && submit){
            createUser({
                variables:{
                    input:{
                        data:data.user
                    }
                }
            });
        }
    },[submit]);

    const checkUser = (data) => {
        setChecking(true);
        getUsers({
            variables:{
                where:{
                    _or:[{username:data.username},{email:data.email}]
                }
            }
        });
    }

    const onFinish = values => {
        const data_form = formatDataForm(values);
        if(data_form.open_hours){
            data_form.open_hours = moment(data_form.open_hours).format(format);
        }
        if(data_form.close_hours){
            data_form.close_hours = moment(data_form.close_hours).format(format);
        }
        if(data_form.longitude){
            data_form.longitude = parseFloat(data_form.longitude);
        }
        if(data_form.latitude){
            data_form.latitude = parseFloat(data_form.latitude);
        }

        setData(data => ({...data_form,image:image,paypay_qrcode:paypay_qrcode}));
        checkUser(data_form.user);
    };

    return <UpdateForm object={false} onFinish={onFinish} loading={loading} setImage={setImage} setPaypayQrcode={setPaypayQrcode} action="Create Id"/>
}
export default ShopCreate;