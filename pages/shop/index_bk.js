import SHOP_LIST from "../../queries/shop/list";
import React, {useEffect, useState} from "react";
import Wrapper from "../../components/Wrapper";
import List from "../../components/Shop/List";
import {Layout, Select} from 'antd';
const { Content } = Layout;
import { Form, Row, Col, Input, Button } from 'antd';
import $ from "jquery";
import {useLazyQuery, useMutation, useQuery} from "@apollo/client";
import Link from "next/link";
import { PlusCircleOutlined,SearchOutlined } from '@ant-design/icons';

const { Option } = Select;
const Shop = () => {
    const where_default = {
        supplier:true
    }
    const pagination_default = {
        current: 1,
        pageSize: process.env.pageSize??10,
        total:0
    }

    const where_fields = {
        "user_id[username_contains]":'Username',
        name_contains:'Name',
        phone_contains:'Phone',
    }
    const [form] = Form.useForm();
    const [where,setWhere] = useState(where_default);
    const [pagination,setPagination] = useState(pagination_default);
    const [reload,setReload] = useState(false);

    const [getPartners, { called, loading, data}] = useLazyQuery(SHOP_LIST,
        { variables: { limit:pagination.pageSize,start:(pagination.current - 1) * pagination.pageSize,where: where },fetchPolicy: 'network-only' }
        );
    useEffect(() => {
        if(!called){
            getPartners();
        }
    },[called]);

    useEffect(() => {
        if(reload){
            setPagination(pagination_default);
            getPartners({ variables: { limit:pagination_default.pageSize,start:(pagination_default.current - 1) * pagination_default.pageSize,where: where } });
        }
        setReload(false);
    },[reload]);

    useEffect(() => {
        setPagination({...pagination, current : 1});
    },[where]);
    useEffect(() => {
        if(!$.isEmptyObject(data) && !loading){
            setPagination({...pagination,total:data.partnersConnection.aggregate.count});
        }
    },[data,loading]);

    const onFinish = values => {
        let new_where = where_default;
        Object.entries(values).forEach(entry => {
            let [key, value] = entry;
            if(value!= undefined && value != ""){
                let index_sub = key.indexOf("[");
                if(index_sub > 0){
                    let child = key.substring(index_sub + 1,key.indexOf("]"));
                    key = key.replace(/\[.*\]/,'');
                    let tmp = {};
                    tmp[child] = value;
                    new_where[key] = tmp;
                }else{
                    new_where[key] = value;
                }
            }
        });
        setWhere(new_where);
    };

    const onClear = () =>{
        form.resetFields();
        setWhere(where_default);
    }

    const getFields = () => {
        const children = [];

        Object.entries(where_fields).forEach(entry => {
            const [key, label] = entry;
            children.push(
                <Col span={6} key={key}>
                    <Form.Item
                        name={key}
                    >
                        <Input placeholder={label} />
                    </Form.Item>
                </Col>,
            );
        });
        return children;
    };

    return (
        <Wrapper>
            <Content>
                <main>
                    <div className="container-fluid">
                        <h2 className="mt-30 page-title">Shops</h2>
                        <ol className="breadcrumb mb-30">
                            <li className="breadcrumb-item">Shops</li>
                            <li className="breadcrumb-item active">List</li>
                        </ol>
                        <div className="row justify-content-between">

                            <div className="col-lg-12 col-md-12 mt-30">
                                <Form
                                    form={form}
                                    name="advanced_search"
                                    className="ant-advanced-search-form"
                                    onFinish={onFinish}
                                >
                                    <Row>
                                        <Col span={6}>
                                            <Form.Item
                                                name="active"
                                            >
                                                <Select
                                                    showSearch
                                                    placeholder="Select a status"
                                                    // optionFilterProp="children"
                                                    filterOption={(input, option) =>
                                                        option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                                    }
                                                >
                                                    <Option value="">All</Option>
                                                    <Option value="1">Active</Option>
                                                    <Option value="0">Inactive</Option>
                                                </Select>
                                            </Form.Item>
                                        </Col>
                                    </Row>
                                    <Row gutter={2}>{getFields()}

                                    </Row>
                                    <Row>
                                        <Col span={24} style={{ textAlign: 'right',}}>
                                            <Button type="primary" htmlType="submit" className="status-btn hover-btn custom-mg-icon" icon={<SearchOutlined />}>Search</Button>
                                            <Button style={{ margin: '0 8px'}} onClick={onClear}>Clear</Button>
                                        </Col>
                                    </Row>
                                </Form>
                            </div>
                            <div className="col-lg-12 col-md-12 mt-30">
                                <Col>
                                    <Link href="/shop/create">
                                        <Button  type="primary" style={{ background: "#87d068", borderColor: "#87d068"}} className="custom-mg-icon" icon={<PlusCircleOutlined />} >
                                            Add new
                                        </Button>
                                    </Link>
                                </Col>
                            </div>

                            <div className="col-lg-12 col-md-12">
                                <div className="card card-static-2 mb-30">
                                    <div className="card-title-2 text-right " style={{ textAlign: 'right',}}>

                                    </div>
                                    <div className="card-body-table">
                                        <div className="table-responsive col-lg-12 col-md-12">
                                            <List partners={data} setPagination={setPagination} pagination={pagination} loadingParent={loading} setReload={setReload}/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </Content>





            {/*<Content style={{ margin: '0 16px' }}>*/}
            {/*    <Breadcrumb style={{ margin: '16px 0' }}>*/}
            {/*        <Breadcrumb.Item>Shop</Breadcrumb.Item>*/}
            {/*        <Breadcrumb.Item>List Shop</Breadcrumb.Item>*/}
            {/*    </Breadcrumb>*/}
            {/*    <div style={{ padding: 24 }}>*/}
            {/*        <Form*/}
            {/*            form={form}*/}
            {/*            name="advanced_search"*/}
            {/*            className="ant-advanced-search-form"*/}
            {/*            onFinish={onFinish}*/}
            {/*        >*/}
            {/*            <Row gutter={24}>{getFields()}</Row>*/}
            {/*            <Row>*/}
            {/*                <Col span={24} style={{ textAlign: 'right',}}>*/}
            {/*                    <Button type="primary" htmlType="submit">Search</Button>*/}
            {/*                    <Button style={{ margin: '0 8px'}} onClick={onClear}>Clear</Button>*/}
            {/*                </Col>*/}
            {/*            </Row>*/}
            {/*        </Form>*/}
            {/*    </div>*/}
            {/*    <div className="site-layout-background" style={{ padding: 24, minHeight: 360 }}>*/}
            {/*        <ListShop query={SHOP_LIST} setPagination={setPagination} pagination={pagination} where={where} setLoading={setLoading}>*/}
            {/*            {({ data: { partners } }) => {*/}
            {/*                return (*/}
            {/*                    <List partners={partners} setPagination={setPagination} pagination={pagination} loading={loading}/>*/}
            {/*                );*/}
            {/*            }}*/}
            {/*        </ListShop>*/}
            {/*    </div>*/}
            {/*</Content>*/}
        </Wrapper>
    )
};

export default Shop;