import gql from "graphql-tag";

export {
    COUNT,
    LIST,
    CREATE,
    UPDATE,
    FIND,
    FIND_WITH_PARENT,
    DELETE
}

const COUNT = gql`
  query PartnersConnection ($where:JSON!){   
      partnersConnection(where:$where){
        aggregate {
            count
        }
      }  
  }
`
const CREATE = gql`
mutation createPartner($input: createPartnerInput!) {  
    createPartner (input:$input){
        partner{
            id
            name
        }      
    }
}`
const UPDATE = gql`
mutation updatePartner($input: updatePartnerInput!) {  
    updatePartner (input:$input){
        partner{
            id
            name
        }      
    }
}`

const LIST = gql`
  query Users ($limit: Int!,$start: Int!,$where:JSON!){   
      users(limit: $limit,start:$start,where:$where){
        id
        username
        email
        partner_id{
            id
            name
            image{
              id
              url
            }
            phone
            active
        }
      },
      usersConnection(where:$where){
        aggregate {
            count
        }
      }  
  }
`;

const LIST_PARTNER = gql`
  query Partners ($limit: Int!,$start: Int!,$where:JSON!){   
      partners(limit: $limit,start:$start,where:$where){
        id
        user_id{
            username
        }
        name
        image{
          id
          url
        }
        phone
        active
      },
      partnersConnection(where:$where){
        aggregate {
            count
        }
      }  
  }
`;
const FIND = gql`
  query Partner ($id: ID!){   
      partner(id: $id){
        id
        user_id{
            id
            username
            email
            confirmed
        }
        name
        image{
          id
          url
          name
        }
        image_description{
          id
          url
          name
        }
        paypay_qrcode{
          id
          url
          name
        }
        phone
        address
        customer
        supplier        
        open_hours
        close_hours
        longitude
        latitude
        active
        censorship        
      } 
  }
`;
const FIND_WITH_PARENT = gql`
  query Partner ($id: ID!){   
      partner(id: $id){
        id
        user_id{
            id
            username
            email
            confirmed
        }
        name
        image{
          id
          url
          name
        }
        image_description{
          id
          url
          name
        }
        paypay_qrcode{
          id
          url
          name
        }
        phone
        address
        customer
        supplier        
        open_hours
        close_hours
        longitude
        latitude
        active
        censorship        
        parent_id{
            id
            user_id{
                id
                username
                email
                confirmed
            }
            name
            image{
              id
              url
              name
            }
            image_description{
              id
              url
              name
            }
            paypay_qrcode{
              id
              url
              name
            }
            phone
            address
            customer
            supplier        
            open_hours
            close_hours
            longitude
            latitude
            active
            censorship
        }
      } 
  }
`;
const DELETE = gql`
  mutation deletePartner($id: ID!) {
    deletePartner (input:{ where: { id: $id } }){
        partner{
            id
            name
        }      
    }
}`