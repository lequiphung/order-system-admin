import gql from "graphql-tag";

export {
    LIST,
    FIND_WITH_PARENT,
    UPDATE,
    DELETE
}
const LIST = gql`
  query Products ($limit: Int!,$start: Int!,$where:JSON!){   
      products(limit: $limit,start:$start,where:$where){
        id
        name
        price      
        image{
          id
          url
        }
        active
        censorship
        user_id{
            id
            username
            partner_id{
                id
                name
                image{
                  id
                  url
                }
            }
        }        
      },
      productsConnection(where:$where){
        aggregate {
            count
        }
      }  
  }
`;

const FIND_WITH_PARENT = gql`
  query Product ($id: ID!){   
      product(id:$id){
        id
        name
        price      
        image{
          id
          url
        }
        active
        censorship
        user_id{
            id
            username
            partner_id{
                id
                name
                image{
                  id
                  url
                }
            }
        }
        
        parent_id{
            id
            name
            price      
            image{
              id
              url
            }
            active
            censorship
            user_id{
                id
                username
                partner_id{
                    id
                    name
                    image{
                      id
                      url
                    }
                }
            }        
        }        
      }
  }
`;
const UPDATE = gql`
  mutation updateProduct($input: updateProductInput!) {
    updateProduct (input:$input){
        product{
            id
            name
        }      
    }
}`;

const DELETE = gql`
  mutation deleteProduct($id: ID!) {
    deleteProduct (input:{ where: { id: $id } }){
        product{
            id
            name
        }      
    }
}`;
