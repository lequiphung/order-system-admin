import gql from "graphql-tag";

const SHOP_COUNT = gql`
  query PartnersConnection ($where:JSON!){   
      partnersConnection(where:$where){
        aggregate {
            count
        }
      }  
  }
`;

export default SHOP_COUNT;
