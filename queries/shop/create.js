import gql from "graphql-tag";

const SHOP_CREATE = gql`
mutation createPartner($input: createPartnerInput!) {  
    createPartner (input:$input){
        partner{
            id
            name
        }      
    }
}`

export default SHOP_CREATE;
