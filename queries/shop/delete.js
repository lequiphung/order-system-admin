import gql from "graphql-tag";

const SHOP_DELETE = gql`
  mutation deletePartner($id: ID!) {
    deletePartner (input:{ where: { id: $id } }){
    partner{
        id
      name
    }
      
    }
}`

export default SHOP_DELETE;
