import gql from "graphql-tag";

const SHOP_FIND = gql`
  query Partner ($id: ID!){   
      partner(id: $id){
        id
        user_id{
            id
            username
            email
            confirmed
        }
        name
        image{
          id
          url
          name
        }
        image_description{
          id
          url
          name
        }
        paypay_qrcode{
          id
          url
          name
        }
        phone
        address
        customer
        supplier        
        open_hours
        close_hours
        longitude
        latitude
        active
        censorship        
      } 
  }
`;

export default SHOP_FIND;
