import gql from "graphql-tag";

const SHOP_LIST = gql`
  query Partners ($limit: Int!,$start: Int!,$where:JSON!){   
      partners(limit: $limit,start:$start,where:$where){
        id
        user_id{
            username
        }
        name
        image{
          id
          url
        }
        phone
        active
      },
      partnersConnection(where:$where){
        aggregate {
            count
        }
      }  
  }
`;

export default SHOP_LIST;
