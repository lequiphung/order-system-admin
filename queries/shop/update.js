import gql from "graphql-tag";

const SHOP_UPDATE = gql`
mutation updatePartner($input: updatePartnerInput!) {  
    updatePartner (input:$input){
        partner{
            id
            name
        }      
    }
}`

export default SHOP_UPDATE;
