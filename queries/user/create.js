import gql from "graphql-tag";

const USER_CREATE = gql`
  mutation createUser($input: createUserInput!) {
    createUser (input:$input){
        user{
            id
            username
        }      
    }
}`

export default USER_CREATE;
