import gql from "graphql-tag";

const USER_LIST = gql`
  query Users ($where:JSON!){   
      users(limit:1,start:0,where:$where){
        id
        username
      }
  }
`;

export default USER_LIST;
