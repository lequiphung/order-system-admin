import gql from "graphql-tag";

export {
    FIND,
    FIND_WITH_PARTNER_PARENT,
    DELETE,
    LOGIN
}

const FIND = gql`
  query User ($id: ID!){   
      user(id: $id){
        id
        username
        email
        confirmed
        partner_id{
            id
            name
            image{
              id
              url
              name
            }
            image_description{
              id
              url
              name
            }
            paypay_qrcode{
              id
              url
              name
            }
            phone
            address
            customer
            supplier        
            open_hours
            close_hours
            longitude
            latitude
            active
            censorship  
        }
      } 
  }
`;

const DELETE = gql`
  mutation deleteUser($id: ID!) {
    deleteUser (input:{ where: { id: $id } }){
        user{
            id
            username
        }      
    }
}`;

const FIND_WITH_PARTNER_PARENT = gql`
  query User ($id: ID!){   
      user(id: $id){
        id
        username
        email
        confirmed
        partner_id{
            id
            name
            image{
              id
              url
              name
            }
            image_description{
              id
              url
              name
            }
            paypay_qrcode{
              id
              url
              name
            }
            phone
            address
            customer
            supplier        
            open_hours
            close_hours
            longitude
            latitude
            active
            censorship
            parent_id{
                id
                name
                image{
                  id
                  url
                  name
                }
                image_description{
                  id
                  url
                  name
                }
                paypay_qrcode{
                  id
                  url
                  name
                }
                phone
                address
                customer
                supplier        
                open_hours
                close_hours
                longitude
                latitude
                active
                censorship
            }  
        }
      } 
  }
`;

const LOGIN = gql`
mutation Login($input: UsersPermissionsLoginInput!) {
    login(input: $input) {
    jwt
    user {
            id
            email
            role {
                id
                name
                description
                type
            }
        }
    }
}`