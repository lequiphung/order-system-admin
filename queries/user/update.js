import gql from "graphql-tag";

const USER_UPDATE = gql`
  mutation updateUser($input: updateUserInput!) {
    updateUser (input:$input){
        user{
            id
            username
        }      
    }
}`

export default USER_UPDATE;
