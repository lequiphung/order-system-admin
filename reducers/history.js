import * as types from '../constants/types'

const initialState = {
    history: []
};

// REDUCERS
export const routeHistoryReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.ROUTE_HISTORY:
            return {
                ...state,
                history: [...state.history,action.pathname]
            }
        default:
            return state
    }
}