import * as loginReducer from './loginReducer';
import * as CensorShip from './censorship';
import * as partnerReducer from "./partnerReducer";
import * as routeHistoryReducer from "./history";
export default Object.assign(loginReducer,CensorShip,partnerReducer,routeHistoryReducer);