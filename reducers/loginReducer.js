import * as types from '../constants/types'

const initialState = {
  userInfo: null,
  token: null
};

// REDUCERS
export const loginReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.LOGIN_SUCCESS:
      return {
        ...state,
        token: action?.userInfo?.jwt,
        userInfo: action?.userInfo?.user
      }
    default:
      return state
  }
}