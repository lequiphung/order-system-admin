import * as types from "../constants/types";

const initialState = {
    object_info: {
        title:'',
        action:'',
        link_back: '',
        customer:false,
        shop:false
    }
};

export const partnerReducer = (state=initialState, action) => {
    switch (action.type) {
        case types.PARTNER_SET_OBJECT_INFO:
            return {
                ...state,
                object_info:action.object_info
            }
        default:
            return state
    }
}