/**
 *  Redux saga class init
 */
import { takeEvery, all, takeLatest } from 'redux-saga/effects'
import * as types from '../constants/types'
import { loginRequest } from './loginSaga'
import {helloCensorship} from './censorship/shop';
import {requestPartnerSetObjectInfo} from "./partnerSaga";


export default function* watch() {
  yield all([
    takeEvery(types.LOGIN_REQUEST, loginRequest),
    takeEvery('CENSORSHIP', helloCensorship),
    takeEvery(types.PARTNER_REQUEST_SET_OBJECT_INFO, requestPartnerSetObjectInfo)

  ]);
}