import { put, call } from 'redux-saga/effects'
import { loginUser } from '../services/userServices'
import * as loginActions from '../actions/loginActions'

// Our worker Saga that logins the user
export function* loginRequest(action) {
  console.log(action)
  const response = yield call(loginUser, action.username, action.password)

  if (response.data.login) {
    yield put(loginActions.loginSuccess( response.data.login ))
  } else {
    yield put(loginActions.loginFailed((response.data)))
  }
}