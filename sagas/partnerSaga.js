import {call, put} from "redux-saga/effects";

import {useDispatch} from "react-redux";
//import {setObjectInfo} from "../actions/partnerAction";
import * as partnerAction from "../actions/partnerAction";

export function* requestPartnerSetObjectInfo(action) {

    //alert('Hello !');
    console.log(action);

    yield put (partnerAction.setObjectInfo(action.object_info));
    //dispatch({ type: 'INCREMENT' });
}