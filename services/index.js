import React from "react";
import {useQuery} from "@apollo/client";
import {indexServices} from "./indexService";
import axios from "axios";

const CallApi = ({ children, url, method }) => {
    const data = axios.get(url)
        .then(res => {
            console.log(data);
            return res.data;
        })

    //const data = indexServices.callApi(url,method);
    return children({ data });
};

export default CallApi;
