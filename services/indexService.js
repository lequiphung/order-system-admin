import axios from 'axios';

export const indexServices = {
    callApi
};


function callApi(url,method='GET'){
    const requestOptions = {
        method: method
    };

    // return axios.get(url)
    //     .then(res => {
    //         const data = res.data;
    //         return data;
    //     })

    return fetch(url, requestOptions)
        .then(handleResponse)
        .then(data => {
            console.log(data);

            return data;
        });
}
function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                // auto logout if 401 response returned from api
                return 0;
            }

            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }

        return data;
    });
}