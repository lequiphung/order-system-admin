import { gql, useQuery, useMutation } from '@apollo/client'

export const loginAction = {
    loginUser
};

function loginUser  (identifier, password) {
    const graph = gql`
                    mutation Login($input: UsersPermissionsLoginInput!) {
                        login(input: $input) {
                        jwt
                        user {
                                id
                                email
                                role {
                                    id
                                    name
                                    description
                                    type
                                }
                            }
                        }
                    }`
    const [login, { data }] = useMutation(graph)
    login({ variables: {
            input: {
                identifier,
                password,
                provider: 'local'
            }
        }})
    return data
}

