import { useMemo } from 'react'
import { createStore, applyMiddleware, compose } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import { persistReducer, persistCombineReducers } from 'redux-persist'
import createSagaMiddleware from 'redux-saga'
import storage from 'redux-persist/lib/storage'
import reducer from './reducers'
import rootSaga from './sagas'

let store

const persistConfig = {
  key: 'primary',
  storage,
  whitelist: ['loginReducer'], // place to select which state you want to persist
}

const middleware = []
const sagaMiddleware = createSagaMiddleware()

middleware.push(sagaMiddleware)

const enhancers = [applyMiddleware(...middleware)]

const persistedReducer = persistCombineReducers(persistConfig, reducer)

function makeStore(initialState) {
  return createStore(
    persistedReducer,
    initialState,
    compose(...enhancers)
  )
}

export const initializeStore = (preloadedState) => {
  let _store = store ?? makeStore(preloadedState)

  // After navigating to a page with an initial Redux state, merge that state
  // with the current state in the store, and create a new store
  if (preloadedState && store) {
    _store = makeStore({
      ...store.getState(),
      ...preloadedState,
    })
    // Reset the current store
    store = undefined
  }

  sagaMiddleware.run(rootSaga)
  
  // For SSG and SSR always create a new store
  if (typeof window === 'undefined') return _store
  // Create the store once in the client
  if (!store) store = _store

  return _store
}

export function useStore(initialState) {
  const store = useMemo(() => initializeStore(initialState), [initialState])
  
  return store
}
